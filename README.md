lab-audit
=========

Source tree setup
    git clone ...
    npm install

Compile the code from src/ to build/
    node_modules/.bin/tsc

Bundle the code to as lab-view-dev/index.js
    node_modules/bin/rollup -c

Deploying
    java -jar WebViewSync_3.5.2.jar sync -a