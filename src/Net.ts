import * as Model from './Model';
import * as _localforage from 'localforage';
import * as Bluebird from 'bluebird';
import { throws } from 'assert';

const localforage = (_localforage as any).default as typeof _localforage;
const RETRY_DELAY  = 4000 as number;
const PROMISE_TIMEOUT = 15000 as number

export async function getJSON(url: string) {
  const data = await fetch(url, { credentials: 'same-origin', });
  const result = await data.json()
  return result
}

function jsonPromise(url: string, options:{} = {}): Promise<any>{
  return fetch(url, options)
}

function jsonPromiseRetry(url: string, options:{} = {}): Promise<any>{
  return jsonPromise(url, options)
    .catch(() => {
      return promiseDelay()
        .then(() => {
          return jsonPromise(url, options)
        })
    })
}

async function getJSONPromiseRetry(url: string){
  const data = await jsonPromiseRetry(url, {credentials: 'same-origin'})
  return await data.json()
}


async function postJsonPromiseRetry(url: string, body: string = ''){
  const options = {
    body,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json'
    },
    credentials: 'same-origin',
    method: 'PUT',
  }
  return jsonPromiseRetry(url, options)
}




function timeoutPromise(): Promise<any>{
  return new Promise((_, reject) => {
    setTimeout(() => reject(new Error('Timeout')), PROMISE_TIMEOUT) 
  })
}

export async function getJSONPromise(url: string): Promise<any>{
  return fetch(url, {credentials: 'same-origin', })
          .catch(() => {
            return promiseDelay(RETRY_DELAY)
              .then(() => {
                return getJSONPromise(url)
              })
          })
}

export async function putJSON(url: string, body: string = '') {
  const data = await fetch(url, {
    body,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json'
    },
    credentials: 'same-origin',
    method: 'PUT',
  });
  const result = await data.json()
  return result
}

export function putJSONPromise(url: string, body: string = '') {
  const putPromise = fetch(url, {
        body,
        headers: {
          accept: 'application/json',
          'content-type': 'application/json'
        },
        credentials: 'same-origin',
        method: 'PUT',
      })
  
  return Promise.race([putPromise, timeoutPromise()])
    .then((response) => {
      response.json()
    });
}

export async function postJSON(url: string, body: string = '') {
  const data = await fetch(url, {
    body,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json'
    },
    credentials: 'same-origin',
    method: 'POST',
  });
  const result = await data.json()
  return result
}

export function postJSONPromise(url: string, body: string = ''): Promise<any> {
  
  const dataPromise = fetch(url, {
    body,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json'
    },
    credentials: 'same-origin',
    method: 'POST',
  })

  return Promise.race([dataPromise, timeoutPromise()])
    .then((response) => {
      return response.json()
    })
    .catch(() => {
      return promiseDelay(RETRY_DELAY)
        .then(() => {
          return postJSONPromise(url, body)
        })
    })
}

export function postJSONPromiseNoRetry(url: string, body: string = ''): Promise<any> {
  return Promise.race([fetch(url, {
      body,
      headers: {
        accept: 'application/json',
        'content-type': 'application/json'
      },
      credentials: 'same-origin',
      method: 'POST',
    }),
    timeoutPromise()]
  )
  .then((response) => {
    return response.json()
  })
}



export async function sessionExpirationExtension() :Promise<any>{

  return fetch(`/SessionExpiration?action=update`, { credentials: 'same-origin', })
          // .then(response => console.log(response))
          .catch(() => {
            return promiseDelay(RETRY_DELAY)
                    .then(() => sessionExpirationExtension())
          })
}

export function uploadImage(image: ArrayBuffer): any {
  
  const uuid = (function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return Date.now().toString() + '-' + s4() + '-' + s4();
  })();

  const dataView = new DataView(image);
  const blob = new Blob([dataView], { type: 'image/jpeg' });
  const file = new File([blob], "observationImage" + uuid + ".jpeg");
  const formData = new FormData();
  formData.append("file", file);

  return fetch(`/p/fileupload/uploadimage`, {
    body: formData,
    credentials: 'same-origin',
    method: 'POST',
  }).then((response) => {
    return response.json()
  })
  .catch(() => {
    return promiseDelay(RETRY_DELAY)
      .then(() => {
        return uploadImage(image)
      })
  })
}

export async function getBuildings() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/buildings`);
  const result = data.data as Model.Building[]
  result.sort((a, b) => {
    const digitA = Number(a.name.split(" ")[1]);
    const digitB = Number(b.name.split(" ")[1]);
    if (isNaN(digitA)) {
      return 1
    }
    else if (isNaN(digitB)) {
      return -1
    }
    else if (digitA > digitB) {
      return 1
    }
    else if (digitA < digitB) {
      return -1
    }
    return 0;
  })
  return result
}

export async function getFloors(buildingId: string) {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/buildings/${buildingId}/floors`);
  return data.data as Model.Floor[];
}

export async function getCurrentUser() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/currentUser`);
  return data.data as Model.User;
}

export async function getRiskLevels() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/riskLevel`);
  return data.data as Model.RiskLevel[];
}

export async function getWorkOrderActions() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/workOrderAction`);
  return data.data as Model.WorkOrderAction[];
}

export async function getResponsiblePersons() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/employees?query=true`);
  return data.data as Model.ResponsiblePerson[];
}

export async function getRequestClasses() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/requestClass`);
  return data.data as Model.RequestClass[];
}

export async function getOrganizations() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/organizations`);
  const rawData = data.data as Model.Organization[];

  const groupedObject = {}

  for (let org of rawData) {

    if (groupedObject[org._id]) {
      groupedObject[org._id].space.push(org.space);
    }
    else {
      groupedObject[org._id] = { id: org.id, _id: org._id, space: [org.space], name: org.name }
    }
  }
  const result = [] as Model.Organization[]
  for (let key in groupedObject) {
    result.push(groupedObject[key]);
  }
  result.sort((a, b) => a.name.localeCompare(b.name))
  return result as Model.Organization[];
}

export async function getInspections() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/myInspections`);
  return data.data as Model.Inspection[];
}

export async function postInspection(inspection: Model.NewInspection): Promise<Model.Inspection> {
  const data = await postJSON(
    `/p/webapi/rest/v2/LabAudit/-1/myInspections?actionGroup=default&action=create`,
    JSON.stringify({ data: inspection }));
  return {
    ...inspection,
    _id: data.createdRecordId
  };
}

export async function signOut() {

  return fetch('/p/websignon/signout', {

    headers: {
      accept: 'application/json',
      'content-type': 'application/json'
    },
    credentials: 'same-origin',
    method: 'POST',
  });
}

export async function retireInspection(inspection: Model.Inspection) {
  return await putJSON(
    `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspection._id}?actionGroup=default&action=cstRetire`,
    JSON.stringify({ data: inspection }));
}

export async function promiseDelay(time?: number): Promise<number>{
  time = time && RETRY_DELAY;
  return new Promise(resolve => setTimeout(resolve, time))
}

export async function putQuestion(inspectionId: string, question: Model.Question): Promise<any> {
  return putJSONPromise(
    `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${question._id}`,
    JSON.stringify({ data: question }))
    .catch(() => {
      return promiseDelay(RETRY_DELAY)
        .then(() => {
          return putQuestion(inspectionId, question)
        })
    });
}

export async function putInspection(inspectionId: string, inspection: Model.Inspection) {
  return putJSON(
    `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}`,
    JSON.stringify({ data: inspection }));
}

export async function postObsImage(inspectionId: string, questionId: string, workTaskId: string, image: Model.NewImage): Promise<{}> {
  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${questionId}/workTask/${workTaskId}/obsImages?actionGroup=default&action=create`;
  return postJSONPromiseNoRetry(url, JSON.stringify({ data: image }))
      .catch(() => {
        return promiseDelay(RETRY_DELAY)
          .then(async () => {
            // returns [{imageSource: .., _id: ..}, {}]
            const associatedImages = await getObservationImages(inspectionId, questionId, workTaskId)
            const responseData = await associatedImages.json()
            const imageSourceData = responseData.data
            const uploadedFiles = imageSourceData.map((obj: any) => obj.imageSource)

            // Determine if image association was already made
            const fileAlreadyUploaded = uploadedFiles.filter((imgSource :any) => imgSource == image.imageSource)
            if(fileAlreadyUploaded) {
              return Promise.resolve()
            } else {
              return postObsImage(inspectionId, questionId, workTaskId, image)
            }
          })
      });
}


async function getObservationImages(inspectionId: string, questionId: string, workTaskId: string): Promise<any> {
  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${questionId}/workTask/${workTaskId}/obsImageQuery`;
  return getJSONPromise(url) 
}

async function getQuestionObservations(inspectionId: string, questionId: string): Promise<any> {
  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${questionId}/observationsAttachedToQuestion`;
  const query = await getJSONPromise(url) 
  const data = await query.json()
  return data
}

async function createObservation(inspection: Model.Inspection, observation: [Model.Question, Model.NewWorkTask]): Promise<any>{
  return postJSONPromiseNoRetry(`/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspection._id}/Questions/${observation[0]._id}/workTask?actionGroup=default&action=create`,
    //@ts-ignore
    JSON.stringify({ data: observation[1] })) 
  .catch(()=>{
    return promiseDelay(RETRY_DELAY)
      .then(async () => {
        const observationID = observation[1].id

        const query = await getQuestionObservations(inspection._id, observation[0]._id)
        // const queryResults = await query.json()
        const filteredQuerys = query.data.filter((queryObservation: any) => queryObservation.uniqueID ==  observationID)

        if (filteredQuerys.length > 0){
          return {createdRecordId: filteredQuerys[0].systemRecordID}
        } else {
          return createObservation(inspection, observation)
        }
      
      })
  })
}

async function putObservation(inspection: Model.Inspection, observation: [Model.Question, Model.NewWorkTask]): Promise<any> {
  const observationResult = await createObservation(inspection, observation) as any

  // This propbably doesnt need to be awaited
  const auditedDepartmentsResult = await addAuditedDepartments(inspection._id, observation[1].auditedOrgs, observation[0]._id, observationResult.createdRecordId)

  // Add images to the observation record
  if (observation[1].images){
    //Get images as strings\
    const images = await localforage.getItem(observation[1].images as string) as any

    for(let i=0; i< images.length;i++){
      // post image to tririga
      let image = images[i] as Model.NewImage
      let imageResponse = await uploadImage(image.imageURL)

      // Add image to the observation
      let imageAssociateResponse = await postObsImage(inspection._id, observation[0]._id, observationResult.createdRecordId, { imageURL: imageResponse.fileURL, imageSource:imageResponse.fileURL })
    }
    return auditedDepartmentsResult
  } 
}

async function sendInspectionCompleteAction(inspection: Model.Inspection): Promise<any> {
    return putJSONPromise(
      `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspection._id}?actionGroup=default&action=complete`,
      JSON.stringify({ data: inspection }))
        .catch(() => {
          return promiseDelay(RETRY_DELAY)
                  .then(() => {
                    return sendInspectionCompleteAction(inspection)
                  })
        });
}

async function manageUpload(func: (s:string, v: any) => Promise<any>, inspectionID: string, arrayData: any[]): Promise<any>{
  const maxSend = 3
  for(let i = 0; i < arrayData.length; i = i + maxSend){
    let promiseArray = [] as Promise<any>[]
    for(let j = 0; j + i < Math.min(arrayData.length, i+maxSend) ; j++){
      promiseArray.push(func(inspectionID, arrayData[i+j]))
    }
    await Promise.all(promiseArray)
  }
  return Promise.resolve()
}

export async function completeInspection(inspection: Model.Inspection, 
                                        selectedSpaces: Model.Space[], 
                                        unselectedSpaces: Model.Space[],
                                        questions: Model.Question[], 
                                        observations: [Model.Question, Model.NewWorkTask][]) {
  
  const selectedSpacesPromise = await Promise.all(selectedSpaces.map(space => addSelectedSpace(inspection._id, space)))
  // const selectedSpacesPromise = await manageUpload(addSelectedSpace,inspection._id, selectedSpaces)

  const unselectedSpacesPromise = await Promise.all(unselectedSpaces.map(space => removeSelectedSpace(inspection._id, space)))
  // const unselectedSpacesPromise = await manageUpload(removeSelectedSpace, inspection._id, unselectedSpaces)

  const questionsPromise = await Promise.all(questions.map(q => putQuestion(inspection._id, q)))
  // const questionsPromise = await manageUpload(putQuestion, inspection._id, questions)

  for (let i = 0; i < observations.length; i++){
    await putObservation(inspection, observations[i])
  }
  // complete the inspection, force this to work as well!
  const inspectionCompletionPromise = await sendInspectionCompleteAction(inspection)
  return inspectionCompletionPromise
}


export function addAuditedDepartments(inspectionId: string, auditedOrgs: Model.Organization[], questionId: string, workTaskId: string): Promise<any> {

  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${questionId}/workTask/${workTaskId}/costCenters?type=add`;
  return putJSONPromise(url, JSON.stringify({ data: auditedOrgs }))
    .catch(() => {
        return promiseDelay(RETRY_DELAY)
          .then(() => {
            return addAuditedDepartments(inspectionId, auditedOrgs, questionId, workTaskId)
          })
    });
}

export async function getInspectionQuestions(inspectionId: string) {
  const data = await getJSON(
    `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions`);
  const questions = data.data as Model.Question[];
  for (let question of questions) {
    if (question.hasPRC) {
      const pcrData = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${question._id}/PreCannedResponses`);
      const prc = pcrData.data as Model.PreCannedResponse[];
      question.preCannedResponses = prc;
    }
  }
  return questions
}

export async function getQuestionCategories() {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/questionCategories`);
  return data.data as Model.Category[];
}

export async function getWorkTasks(inspectionId: string, questionId: string) {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${questionId}/workTask`);
  return data.data as Model.WorkTask[];
}

export async function getInspectionQuestionsByCategory(inspectionId: string) {
  const [categories, questions] = await Promise.all(
    [getQuestionCategories(), getInspectionQuestions(inspectionId)]);

  const groupedQuestions: { [category: string]: Model.Question[]; } = {};
  for (let question of questions) {
    if (!groupedQuestions[question.cat])
      groupedQuestions[question.cat] = [];

    groupedQuestions[question.cat].push(question);
  }
  return categories.map(category =>
    groupedQuestions[category.name].
      sort((lhs, rhs) => lhs.num - rhs.num || lhs.text.localeCompare(rhs.text)));
}

export async function putInspectionQuestion(inspectionId: string, question: Model.Question) {
  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/Questions/${question._id}`;
  return fetch(url, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ data: question }),
    credentials: 'same-origin'
  });
}

export async function getInspectionSelectedSpaces(inspectionId: string) {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/selectedSpaces`);
  const selectedSpaces = data.data as Model.Space[];

  const selectedSpacesById: { [id: string]: Model.Space; } = {};
  for (let space of selectedSpaces)
    if (space._id)
      selectedSpacesById[space._id] = space;
  return selectedSpacesById;
}

export async function addOrRemoveSelectedSpace(add: boolean, inspectionId: string, space: Model.Space): Promise<{}> {
  const url = `/p/webapi/rest/v2/LabAudit/-1/myInspections/${inspectionId}/selectedSpaces?type=${add ? 'add' : 'remove'}`;
  return fetch(url, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ data: space }),
    credentials: 'same-origin'
  })
  .catch(() => {
    return promiseDelay(RETRY_DELAY)
      .then(() => {
        return addOrRemoveSelectedSpace(add, inspectionId, space)
      })
  })
  
}

export async function addSelectedSpace(inspectionId: string, space: Model.Space): Promise<{}> {
  return addOrRemoveSelectedSpace(true, inspectionId, space);
}

export async function removeSelectedSpace(inspectionId: string, space: Model.Space): Promise<{}> {
  return addOrRemoveSelectedSpace(false, inspectionId, space);
}

export async function getInspectionSpaces(inspection: Model.Inspection | Model.NewInspection) {
  const data = await getJSON(`/p/webapi/rest/v2/LabAudit/-1/labs`);
  const labs = data.data as Model.Lab[];

  if (inspection.floor && inspection.floor.value) {
    return labs.filter(lab =>
      lab.building.value === inspection.building.value &&
      lab.floor.value === inspection.floor.value);
  } else {
    return labs.filter(lab =>
      lab.building.value === inspection.building.value);
  }
}
