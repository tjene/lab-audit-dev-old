import * as React from 'react';
import * as Model from './Model';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as _localforage from 'localforage';
import ButtonMaterial from '@material-ui/core/Button';

const localforage = (_localforage as any).default as typeof _localforage;

module ButtonComponent {
    export interface ButtonProps {

        onClick: (string: string) => void;
        inspectionId: string
    }
}

class ButtonComponent extends React.PureComponent<ButtonComponent.ButtonProps>{

    private handleClick = () => {

        this.props.onClick(this.props.inspectionId);


    }
    render() {
        return <ButtonMaterial disableRipple color="primary" variant="raised" onClick={this.handleClick}>View Inspection</ButtonMaterial>
    }
}

module LabAuditListItem {
    export interface Props extends RouteComponentProps<any> {
        inspection: Model.Inspection;
    }

    export interface State {
        name: string;
        date: string
    }
}


export class LabAuditListItem extends React.Component<LabAuditListItem.Props, LabAuditListItem.State> {

    private titleStyle: React.CSSProperties = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        fontSize: '12px',
        fontWeight: 400,
        letterSpacing: '0.011em',
        lineHeight: '20px',
        paddingTop: '5px'
    };

    private handleClick = (id: string) => {
        this.props.history.push(`/${id}`);
    }
    state: LabAuditListItem.State = {
        name: '',
        date: ''
    }

    componentDidMount() {
        const inspection = localforage.getItem(`inspection-${this.props.inspection._id}`)
            .then((res: any) => {
                if (res&&res.date) {
                    this.setState({ date: res.date.toLocaleDateString() })
                }
                if (res&&res.name && res.name.length > 0) {
                    this.setState({ name: res.name })
                }
                if (!res) {
                    if (this.props.inspection.inspectionname) {
                        this.setState({ name: this.props.inspection.inspectionname })

                    }
                    if (this.props.inspection.date) {
                        this.setState({ date: this.props.inspection.date })
                    }

                }
            })
    }

    render() {
        return <React.Fragment>
            {this.state.name && this.state.name.length > 0 && <React.Fragment>
                <div style={this.titleStyle}>Name</div>
                <div>{this.state.name}</div>
            </React.Fragment>}
            {this.props.inspection.building && this.props.inspection.building.value && <React.Fragment>
                <div style={this.titleStyle}>Building</div>
                <div>{this.props.inspection.building.value}</div>
            </React.Fragment>}
            {this.props.inspection.multifloor && <React.Fragment>
                <div style={this.titleStyle}>Floor</div>
                <div>Multi-Floor</div>
            </React.Fragment>}
            {!this.props.inspection.multifloor && this.props.inspection.floor && this.props.inspection.floor.value && <React.Fragment>
                <div style={this.titleStyle}>Floor</div>
                <div>{this.props.inspection.floor.value}</div>
            </React.Fragment>}
            <div style={this.titleStyle}>Inspection Start Date</div>
            <div>{this.state.date && new Date(this.state.date).toLocaleDateString()}</div>
            <ButtonComponent onClick={this.handleClick} inspectionId={this.props.inspection._id}></ButtonComponent>
        </React.Fragment>
    }
}

export default withRouter<LabAuditListItem.Props>(LabAuditListItem);