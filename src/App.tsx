///<reference path="../src/index.d.ts"/>

// FIXME
//
// Add 'Pick Audited Departments' selection for observations
//
// Service worker scopes aren't compatible with the hosting paths that tririga
// uses. .js files are served out of
// eg. /p/components/r/<locale>/l/lab-audit-dev/index.js but would need to be
// under /p/web/LabAuditDev/ to work with the service worker scoping rules.

import * as React from 'react';
import { Route, withRouter, RouteComponentProps } from 'react-router-dom';

import * as Model from './Model';
import * as _localforage from 'localforage';
import LabAuditList from './LabAuditList';
import LabAuditDetails from './LabAuditDetails';
import * as Net from './Net';
import {
  Loader, Toolbar, Button,
  HomeIcon, SignOutIcon, Modal
} from './Component';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonMaterial from '@material-ui/core/Button';


const localforage = (_localforage as any).default as typeof _localforage;
const SESSIONEXPIRATIONINTERVAL_MS = 120*1000 as number

Modal.setAppElement('lab-audit');

module App {
  export interface Props extends RouteComponentProps<any> { }
  export interface State {
    inspections: Model.Inspection[];
    currentUser: Model.User | null;
    buildings: Model.Building[];
    floors: Model.Floor[][];
    openDialog: boolean
  }

}

class App extends React.Component<App.Props, App.State>{
  state: App.State = {
    inspections: [],
    currentUser: null,
    buildings: [],
    floors: [],
    openDialog: false
  };

  componentDidMount = async () => {
    setInterval(Net.sessionExpirationExtension, SESSIONEXPIRATIONINTERVAL_MS)
  }

  private handleLoad = async () => {
    const [inspections, currentUser, buildings] =
      await Promise.all([Net.getInspections(), Net.getCurrentUser(),
      Net.getBuildings()]);
    //loop inspections, 
    //if inspection date null, use their id's to get dates from local storage
    //map in date to local object of inspection
    const floors = await Promise.all(buildings.map(b => Net.getFloors(b._id)));

    this.setState({ inspections, currentUser, buildings, floors });
    await localforage.setDriver(localforage.INDEXEDDB);

  }

  private handleHomeClick = () => {
    window.location.href = '../web/Inspections';
  }

  private handleAuditHomeClick = () => {
    this.props.history.push('/');
  }

  private handleNewInspectionClick = () => {
    this.props.history.push('/newInspection');
  }

  private handleSignoutClick = async () => {
    this.setState({openDialog:true});
  }

  private signOut = async () => {

    await Net.signOut();
    window.location.reload(true);

  }

  private handleAddInspection = (newInspection: Model.Inspection) => {

    this.setState({
      inspections: [...this.state.inspections, newInspection],
    });
    this.props.history.push(`/${newInspection._id}`)
  }

  private handleMultiFloorChange = async (inspection: Model.Inspection) => {

    const _inspection = { ...inspection, multifloor: true, floor: { id: null, value: null } } as Model.Inspection;
    let spaces = await Net.getInspectionSpaces(_inspection);
    return spaces as Model.Lab[];



  }
  private handleDialogClose = () => {
    this.setState({ openDialog: false });
  }

  private refreshList = (inspection: Model.Inspection) => {
    const inspections = [...this.state.inspections];
    const index = inspections.findIndex((i, index) => i._id === inspection._id);
    inspections.splice(index, 1);
    this.setState({ inspections: inspections })
    //this.props.history.push('/');
  }

  render() {
    const user = this.state.currentUser;
    const userName = (() => {
      if (!user)
        return '';
      else if (user.firstName && user.lastName)
        return `${user.firstName} ${user.lastName}`;
      else if (user.firstName)
        return user.firstName;
      else if (user.lastName)
        return user.lastName;
      else
        return '';
    })();

    return (
      <Loader load={this.handleLoad}>
        <div>
          <Toolbar>
            <Toolbar.Button>
              <Button onClick={this.handleHomeClick} style={{ width: '44px', height: '64px' }}>
                <HomeIcon style={{ width: '24px', height: '24px', fill: 'white' }} />
              </Button>
            </Toolbar.Button>

            <Toolbar.Button>
              <Button style={{
                height: '64px', color: 'white', padding: '0 10px', fontSize: '16px'
              }}>
                Lab Audit
              </Button>
            </Toolbar.Button>
            <Toolbar.Spacer flexGrow={5} />
            <Route exact={true} path="/" render={() =>
              <Button onClick={this.handleNewInspectionClick} style={{
                height: '64px', color: 'white', padding: '0 10px', fontSize: '16px'
              }}>
                New</Button>
            } />
            <Toolbar.Spacer />
            <Toolbar.Text>{userName}</Toolbar.Text>
            <Toolbar.Button>
              <Button onClick={this.handleSignoutClick} style={{ width: '44px', height: '64px' }}>
                <SignOutIcon style={{ width: '24px', height: '24px', fill: 'white' }} />
              </Button>
            </Toolbar.Button>
          </Toolbar>

          <Route exact={true} path="/" render={() =>
            <React.Fragment>
              <LabAuditList inspections={this.state.inspections}
                onNewInspection={this.handleNewInspectionClick} />
            </React.Fragment>
          } />
          <Route exact={true} path="/:id"
            render={(match) => {
              if (match.match.params.id === "newInspection") {
                return <LabAuditDetails params={match.match.params.id} onCreate={this.handleAddInspection} buildings={this.state.buildings} floors={this.state.floors} />
              }
              for (let i of this.state.inspections)
                if (i._id === match.match.params.id)
                  return <LabAuditDetails handleComplete={this.refreshList} onRetire={this.refreshList} handleMultiFloorChange={this.handleMultiFloorChange} params={match.match.params.id} buildings={this.state.buildings} floors={this.state.floors} inspection={i} />;

              return <div style={{ paddingTop: '44px', display: 'flex' }}>
                <div style={{ margin: 'auto' }}>Invalid Inspection</div>
              </div>;
            }} />
        </div>
        <Dialog open={this.state.openDialog} onClose={this.handleDialogClose}>
          <DialogTitle>
            Are you sure you want to sign out?
          </DialogTitle>
          <DialogActions>
            <ButtonMaterial variant="outlined" onClick={this.handleDialogClose} disableRipple>Cancel</ButtonMaterial>
            <ButtonMaterial variant="raised"  onClick={this.signOut} disableRipple>Confirm</ButtonMaterial>
            {/* {this.state.dialogType === 'cancelCreate' ?
            <ButtonMaterial onClick={this.handleModalClose} disableRipple>Confirm</ButtonMaterial>
            : this.state.dialogType === 'retire' ?
              <ButtonMaterial onClick={this.handleRetireInspection} disableRipple>Confirm</ButtonMaterial>
              : this.state.dialogType === 'overrideImg' ?
                <ButtonMaterial onClick={this.confirmTakePicture} disableRipple>Confirm</ButtonMaterial>
                : this.state.dialogType === 'deleteImg' &&
                <ButtonMaterial onClick={this.deleteImage} disableRipple>Confirm</ButtonMaterial>
          } */}
          </DialogActions>
        </Dialog>
      </Loader>
    );
  }
}

export default withRouter<App.Props>(App) 
