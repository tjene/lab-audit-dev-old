import * as React from 'react';
import * as Model from './Model';
import * as _localforage from 'localforage';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as Net from './Net';
import { Loader, Modal, } from './Component';
import Question from './Question'
import 'react-select/dist/react-select.css';
import '../src/style.css';
import ObservationCreator from './ObservationCreator';
import ObservationsTab from './ObservationsTab';
import ButtonMaterial from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import ModalMaterial from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import SelectMaterial from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';
import { isEqual, cloneDeep } from 'lodash';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';


/*This component is the meat and potato of the app , it manages most of the state needed to be able to conduct an inspection
*/

const localforage = (_localforage as any).default as typeof _localforage; //localStorage declaration


//stateless component to display the creation of observation
const ObservationNotification: React.StatelessComponent = (props) => {
  return (<div style={{ height: '100vh', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
    <div style={{ display: 'block' }}>
      <Typography style={{ display: 'block', marginBottom: 15 }} align="center" variant="title">New Observation Created</Typography>
    </div>
    <div style={{ display: 'block' }}>
      <svg width="77px" height="77px" viewBox="0 0 77 77" version="1.1">
        <title>Complete</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="Group-15" fill="#6AB563" fillRule="nonzero">
            <g id="check-mark">
              <path d="M38.5,0 C17.2714609,0 0,17.2714609 0,38.5 C0,59.7285391 17.2714609,77 38.5,77 C59.7285391,77 77,59.7285391 77,38.5 C77,17.2714609 59.7285391,0 38.5,0 Z M60.4881621,27.4650879 L32.7475586,55.2058418 C32.1208809,55.8325195 31.3000488,56.1457832 30.4792168,56.1457832 C29.6583848,56.1457832 28.8374023,55.8325195 28.210875,55.2058418 L13.3035547,40.2983711 C12.676877,39.6719941 12.676877,38.6562559 13.3035547,38.0297285 L15.5715957,35.7615371 C16.1982734,35.1351602 17.2137109,35.1351602 17.8403887,35.7615371 L30.4792168,48.400666 L55.9513281,22.9282539 C56.5780059,22.301877 57.5934434,22.301877 58.2201211,22.9282539 L60.4881621,25.1964453 C61.1148398,25.8229727 61.1148398,26.8387109 60.4881621,27.4650879 Z" id="Shape"></path>
            </g>
          </g>
        </g>
      </svg>
    </div>
  </div >)
}

const CompletionNotification: React.StatelessComponent = (props) => {
  return (<div style={{ height: '100vh', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
    <div style={{ display: 'block' }}>
      <Typography style={{ display: 'block', marginBottom: 15 }} align="center" variant="title">Lab Audit Inspection<br />has been completed</Typography>
    </div>
    <div style={{ display: 'block' }}>
      <svg width="77px" height="77px" viewBox="0 0 77 77" version="1.1">
        <title>Complete</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="Group-15" fill="#6AB563" fillRule="nonzero">
            <g id="check-mark">
              <path d="M38.5,0 C17.2714609,0 0,17.2714609 0,38.5 C0,59.7285391 17.2714609,77 38.5,77 C59.7285391,77 77,59.7285391 77,38.5 C77,17.2714609 59.7285391,0 38.5,0 Z M60.4881621,27.4650879 L32.7475586,55.2058418 C32.1208809,55.8325195 31.3000488,56.1457832 30.4792168,56.1457832 C29.6583848,56.1457832 28.8374023,55.8325195 28.210875,55.2058418 L13.3035547,40.2983711 C12.676877,39.6719941 12.676877,38.6562559 13.3035547,38.0297285 L15.5715957,35.7615371 C16.1982734,35.1351602 17.2137109,35.1351602 17.8403887,35.7615371 L30.4792168,48.400666 L55.9513281,22.9282539 C56.5780059,22.301877 57.5934434,22.301877 58.2201211,22.9282539 L60.4881621,25.1964453 C61.1148398,25.8229727 61.1148398,26.8387109 60.4881621,27.4650879 Z" id="Shape"></path>
            </g>
          </g>
        </g>
      </svg>
    </div>
  </div >)
}

module TabContainer {
  export interface Props {
    active: boolean
  }
}
class TabContainer extends React.PureComponent<TabContainer.Props>{
  render() {
    return (
      this.props.active ? <div>{this.props.children}</div> : <div></div>
    )
  }

}

module LabAuditDetails {
  export interface Props extends RouteComponentProps<any> {
    inspection?: Model.Inspection;
    buildings?: Model.Building[];
    floors?: Model.Floor[][];
    onCreate?: (inspection: Model.Inspection) => void;
    onRetire?: (inspection: Model.Inspection) => void;
    params?: any
    handleMultiFloorChange?: (inspection: Model.Inspection) => Promise<Model.Lab[]>;
    handleComplete?: (inspection: Model.Inspection) => void
  }



  export interface SavedState {
    groupedQuestions: Model.Question[][];
    availableSpaces: Model.Lab[];
    selectedSpacesById: { [id: string]: Model.Space; };
    riskLevels: Model.RiskLevel[];
    correctionActions: Model.WorkOrderAction[];
    responsiblePersons: Model.ResponsiblePerson[];
    requestClasses: Model.RequestClass[];
    organizations: Model.Organization[];
    groupedObservations: [Model.Question, Model.NewWorkTask][][];
    categories: Model.Category[];
    floorValue: number;
    started: boolean;
    date: Date | null;
    name: string;

  }



  export interface State extends SavedState {

    completingInspection: boolean;
    completedInspection: boolean;
    creatingInspection: boolean;
    addObservationQuestion: Model.Question | null;
    notificationObservation: boolean;
    tabValue: number;
    questionCategoryTab: number;
    searchModalOpen: boolean;
    searchValue: string;

    buildingValue: number;
    floorValue: number;

    multiFloor: boolean;
    displayedFloors: Model.Floor[]
    imageArr: Model.NewImage[],
    incompleteQuestions: any[],
    floorSelectOpen: boolean,
    completeModalOpen: boolean,
    answeredCount: number,
    previewImage: boolean,
    previewUrl: any,
    observationsCount: any,
    questionSnapShot: any[],
    answersComplete: boolean,
    openDialog: boolean,
    dialogMessage: string,
    dialogType: string,
    deleteIndex: number,
    alertDialog: boolean,
    alertMessage: string
  }
}

class LabAuditDetails extends React.Component<LabAuditDetails.Props, LabAuditDetails.State> {
  private pictureInput: HTMLInputElement | null = null;
  private image: HTMLImageElement | null = null;
  private totalQuestionCount: number = 0;
  private urls = new WeakMap();
  private imagePlaceHolder = <svg width="120px" height="120px" viewBox="0 0 101 101" version="1.1">
    <title>Image PlaceHolder</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-16" transform="translate(2.000000, 2.000000)" strokeWidth="3">
        <g id="Page-1-Copy-16" stroke="#A6A8AB">
          <polyline id="Stroke-1" points="97 90 97 97 90 97"></polyline>
          <path d="M78,97 L13,97" id="Stroke-3" strokeDasharray="10.142,10.142"></path>
          <polyline id="Stroke-5" points="7 97 0 97 0 90"></polyline>
          <path d="M0,78 L0,13" id="Stroke-7" strokeDasharray="10.142,10.142"></path>
          <polyline id="Stroke-9" points="0 7 0 0 7 0"></polyline>
          <path d="M19,0 L84,0" id="Stroke-11" strokeDasharray="10.142,10.142"></path>
          <polyline id="Stroke-13" points="90 0 97 0 97 7"></polyline>
          <path d="M97,19 L97,84" id="Stroke-15" strokeDasharray="10.142,10.142"></path>
        </g>
        <g id="image-copy-20" transform="translate(27.000000, 25.000000)" stroke="#A4A8AB" strokeLinecap="round" strokeLinejoin="round">
          <rect id="Rectangle-path" x="0" y="0" width="44" height="44" rx="2"></rect>
          <circle id="Oval" cx="13.5" cy="13.5" r="3.5"></circle>
          <polyline id="Shape" points="44 29.2727273 31.8125 17 5 44"></polyline>
        </g>
      </g>
    </g>
  </svg>;
  private lastY = 0;
  private deleteIcon = <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1">
    <title>Delete Image</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-17" transform="translate(-2.000000, -2.000000)">
        <circle id="Oval-2" fill="#FFFFFF" fillRule="nonzero" cx="11.5" cy="12.5" r="7.5"></circle>
        <g id="ic_cancel_24px">
          <g id="Group">
            <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
            <path d="M12,2 C6.47,2 2,6.47 2,12 C2,17.53 6.47,22 12,22 C17.53,22 22,17.53 22,12 C22,6.47 17.53,2 12,2 L12,2 Z M17,15.59 L15.59,17 L12,13.41 L8.41,17 L7,15.59 L10.59,12 L7,8.41 L8.41,7 L12,10.59 L15.59,7 L17,8.41 L13.41,12 L17,15.59 L17,15.59 Z" id="Shape" fill="#FF0000"></path>
          </g>
        </g>
      </g>
    </g>
  </svg>
  state: LabAuditDetails.State = {
    groupedQuestions: [],
    selectedSpacesById: {},
    availableSpaces: [],
    riskLevels: [],
    correctionActions: [],
    responsiblePersons: [],
    requestClasses: [],
    organizations: [],
    categories: [],
    floorValue: -1,
    groupedObservations: [],
    started: false,
    date: null,
    name: '',

    //NON-SAVED STATES
    buildingValue: -1,
    openDialog: false,
    dialogMessage: '',
    multiFloor: false,

    completingInspection: false,
    creatingInspection: false,
    addObservationQuestion: null,
    searchValue: '',
    notificationObservation: false,
    searchModalOpen: false,
    tabValue: 0,
    questionCategoryTab: 1,

    displayedFloors: [],
    imageArr: [],
    floorSelectOpen: false,
    completeModalOpen: false,
    incompleteQuestions: [],
    answeredCount: 0,
    completedInspection: false,
    previewImage: false,
    previewUrl: '',
    observationsCount: {},
    questionSnapShot: [],
    answersComplete: false,
    dialogType: '',
    deleteIndex: -1,
    alertDialog: false,
    alertMessage: ''
  };


  // shouldComponentUpdate(nextProps:any, nextState:any){
  //   if (isEqual(nextProps,this.props) && isEqual(nextState,this.state)){
  //       return false;
  //   } else {
  //       return true;
  //   }
  // }
  private saveState = () => {
    const state = this.state;
    const savedStateWithoutObservations: any = {
      groupedQuestions: state.groupedQuestions,
      selectedSpacesById: state.selectedSpacesById,
      availableSpaces: state.availableSpaces,
      riskLevels: state.riskLevels,
      correctionActions: state.correctionActions,
      responsiblePersons: state.responsiblePersons,
      requestClasses: state.requestClasses,
      organizations: state.organizations,
      categories: state.categories,
      floorValue: state.floorValue,
      started: state.started,
      date: state.date,
      name: state.name
    };

    if (this.props.inspection)
      localforage.setItem(`inspection-${this.props.inspection._id}`, savedStateWithoutObservations);

  }

  private saveStateObservations = (observation: any): Promise<any> | undefined => {
    if (this.props.inspection) {
      return localforage.setItem(`inspection-${this.props.inspection._id}-observations-${observation[1].id}`, observation);
    }
    return undefined
  }
  private deleteSavedStateObservations = (observation: any): Promise<any> | undefined => {
    if (this.props.inspection) {
      let _promises = [];
      if (observation[1].images) {
        _promises.push(localforage.removeItem(`inspection-${this.props.inspection._id}-observations-${observation[1].id}-images`))
      }
      _promises.push(localforage.removeItem(`inspection-${this.props.inspection._id}-observations-${observation[1].id}`))
      return Promise.all(_promises)

    }
    return undefined;
  }

  private saveStateObsImages = (images: any, imagesId: any): Promise<any> | undefined => {
    if (this.props.inspection) {
      return localforage.setItem(imagesId, images)
    }
    return undefined;
  }

  componentDidMount() {
    //loaded inspections

    const _inspection = this.props.inspection;
    let _buildingValue = _inspection ? -1 : 0;

    if (_inspection && _inspection.started && _inspection.date) {
      this.setState({ started: _inspection.started, date: new Date(_inspection.date) });

    }
    if (_inspection && _inspection.inspectionname) {
      this.setState({ name: _inspection.inspectionname });

    }

    if (_inspection && this.props.buildings) {
      _buildingValue = this.props.buildings.findIndex(building => _inspection.building.value === building.name);
      this.setState({ buildingValue: _buildingValue })
    }
    if (this.props.floors && _inspection) {
      const _displayedFloors = [{ name: "Multifloor", _id: -1 }, ...this.props.floors[_buildingValue]] as Model.Floor[];
      const _floorValue = _inspection.multifloor ? 0 : this.props.floors[_buildingValue].findIndex(floor => _inspection.floor.value === floor.name) + 1;
      this.setState({ displayedFloors: _displayedFloors, floorValue: _floorValue })
    }


  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (this.state.tabValue !== prevState.tabValue) {
      const _questionScrollDiv = document.getElementById("questionScrollDiv");
      if (_questionScrollDiv) {
        _questionScrollDiv.removeEventListener("touchstart", this.touchstartEvent)
        _questionScrollDiv.removeEventListener("touchmove", this.touchmoveEvent)
      }
      const _spaceScrollDiv = document.getElementById("spaceScrollDiv");
      if (_spaceScrollDiv) {
        _spaceScrollDiv.removeEventListener("touchstart", this.touchstartEvent)
        _spaceScrollDiv.removeEventListener("touchmove", this.touchmoveEvent)
      }
      if (this.state.tabValue === 1) {
        if (_spaceScrollDiv) {
          _spaceScrollDiv.addEventListener("touchstart", this.touchstartEvent)
          _spaceScrollDiv.addEventListener("touchmove", this.touchmoveEvent)
        }
      } else if (this.state.tabValue === 2) {
        if (_questionScrollDiv) {
          _questionScrollDiv.addEventListener("touchstart", this.touchstartEvent)
          _questionScrollDiv.addEventListener("touchmove", this.touchmoveEvent)
        }
      }
    }
    if (!isEqual(this.state.groupedObservations, prevState.groupedObservations)) {
      this.countObservations();
    }
  }
  private countObservations = () => {

    const observationsCount: { [questionId: string]: number; } = {};// initially empty

    for (let group of this.state.groupedObservations) {
      for (let [question, task] of group) {
        if (!observationsCount[question._id]) //it wasn't there before so zero else add
        {
          observationsCount[question._id] = 0;
        }
        ++observationsCount[question._id];
      }
    }
    this.setState({ observationsCount })
  }

  private resizeImage = (settings: Model.IResizeImageOptions) => {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement('canvas');
    const dataURItoBlob = (dataURI: string) => {
      const bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
        atob(dataURI.split(',')[1]) :
        unescape(dataURI.split(',')[1]);
      const mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
      const max = bytes.length;
      const ia = new Uint8Array(max);
      for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
      return new Blob([ia], { type: mime });
    };
    const resize = () => {

      let width = image.width;
      let height = image.height;
      if (width > height) {
        if (width > maxSize) {
          height *= maxSize / width;
          width = maxSize;
        }
      } else {
        if (height > maxSize) {
          width *= maxSize / height;
          height = maxSize;
        }
      }

      canvas.width = width;
      canvas.height = height;

      const ctx = canvas.getContext('2d')
      if (ctx) {
        ctx.translate(canvas.width / 2, canvas.height / 2)
        const orientation = window.orientation;
        if (orientation === 0) {
          ctx.rotate(Math.PI / 2)

        }
        else if (orientation === -90) {
          ctx.rotate(Math.PI)
        }
        else if (orientation === 180) {
          ctx.rotate(3 * Math.PI / 2)
        }
        ctx.translate(-canvas.width / 2, -canvas.height / 2)
        ctx.drawImage(image, 0, 0, width, height);
      }
      let dataUrl = canvas.toDataURL('image/jpeg', 0.3);
      return dataURItoBlob(dataUrl);

    };

    return new Promise((ok, no) => {
      if (!file.type.match(/image.*/)) {
        no(new Error("Not an image"));
        return;
      }

      reader.onload = (readerEvent: any) => {
        image.onload = () => ok(resize());
        image.src = readerEvent.target.result;
      };
      reader.readAsDataURL(file);
    })
  };

  private touchstartEvent = (e: any) => {
    this.lastY = e.touches[0].clientY;
  }
  private touchmoveEvent = (e: any) => {
    const top = e.touches[0].clientY;
    const scrollTop = e.currentTarget.scrollTop;
    const direction = (this.lastY - top < 0 ? "up" : "down")

    if (scrollTop == 0 && direction === "up") {
      e.preventDefault();
    } else if (scrollTop >= (e.currentTarget.scrollHeight - e.currentTarget.offsetHeight) && direction === "down") {
      e.preventDefault();
    }
  }

  private handleModalClose = () => {
    // if (confirm("Are you sure you want to cancel the observation?")) {
    this.setState({ addObservationQuestion: null });

    // }
  }

  private handleAddObservation = (question: Model.Question) => {

    this.setState({ addObservationQuestion: question });

  }

  private handleQuestionChange = (newQuestion: Model.Question) => {
    // let  observationCountsByQuestionId: { [questionId: string]: number; } = {};// initially empty
    if (this.questionHasObservations(newQuestion._id)) {
      // alert("Please delete observations associated to this question before changing the answer.")
      this.handleAlertDialogOpen("questionChangeObs")
    } else {
      const newGroupedQuestions = this.state.groupedQuestions.map(group => {
        const newGroup: Model.Question[] = [];
        for (let question of group) {
          if (question._id === newQuestion._id)
            newGroup.push(newQuestion);
          else
            newGroup.push(question);
        }
        return newGroup;
      });
      if (this.state.completeModalOpen) {
        this.modifyIncompleteQuestions(newQuestion);
      }
      this.setState({ groupedQuestions: newGroupedQuestions }, this.saveState);
    }
  }

  private questionHasObservations = (id: string) => {
    for (let group of this.state.groupedObservations) {
      for (let [question, task] of group) {
        if (question._id === id)//it wasn't there before so zero else add
        {
          return true
        }
      }
    }
    return false
  }


  private createIncompleteQuestions = () => {
    const questions = this.state.groupedQuestions;
    let incompleteQuestions = [];
    let answeredCount = 0;
    let answersComplete = true;
    for (let group of questions) {
      incompleteQuestions.push(group.filter((q: any) => {
        if (q.resp) {
          answeredCount++;
          return false;
        } else {
          answersComplete = false;
          return true;
        }
      }))
    }
    const snapshot = cloneDeep(incompleteQuestions);

    this.setState({ answersComplete, 
                    incompleteQuestions, 
                    answeredCount, 
                    questionSnapShot: snapshot })
  }
  private modifyIncompleteQuestions = (newQuestion: any) => {
    let answeredCount = this.state.answeredCount;
    let snapshot = cloneDeep(this.state.questionSnapShot);
    let answersComplete = true;
    for (let i = 0; i < this.state.groupedQuestions.length; i++) {
      for (let snapQuestion of snapshot[i]) {

        if (!snapQuestion.resp && (snapQuestion._id !== newQuestion._id || snapQuestion._id === newQuestion._id && newQuestion.resp === "No")) {
          answersComplete = false
        } else if (snapQuestion.resp && snapQuestion._id === newQuestion._id && !newQuestion.resp) {
          answeredCount--;
          answersComplete = false
        } else if (snapQuestion.resp === "No" && !this.questionHasObservations(snapQuestion._id)) {
          answersComplete = false;
        } else if (!snapQuestion.resp && newQuestion.resp) {
          answeredCount++;
        }
        if (newQuestion._id === snapQuestion._id) {
          snapQuestion.resp = newQuestion.resp
        }
      }
    }
    this.setState({ answersComplete, answeredCount, questionSnapShot: snapshot })

  }
  private handleFillSurvey = () => {
    const newGroupedQuestions = this.state.groupedQuestions.map(group => {
      return group.map(q => ({ ...q, resp: 'Yes' }));
    });
    this.setState({ groupedQuestions: newGroupedQuestions }, this.saveState);
  }
  private handleRetireInspection = () => {
    // if (confirm("Are you want to retire this inspection ?")) {
    const observations: [Model.Question, Model.NewWorkTask][] = [];

    for (let group of this.state.groupedObservations)
      for (let observation of group)
        observations.push(observation);
    if (this.props.inspection) {
      localforage.removeItem(`inspection-${this.props.inspection._id}`)
      for (let obs of observations) {
        localforage.removeItem(`inspection-${this.props.inspection._id}-observations-${obs[1].id}`);
        if (obs[1].images) {
          localforage.removeItem(`inspection-${this.props.inspection._id}-observations-${obs[1].id}-images`)
        }
      }
      Net.retireInspection(this.props.inspection)
        .then(() => {
          if (this.props.onRetire && this.props.inspection) {

            this.props.onRetire(this.props.inspection);
          }
        })
      // }
    }

  }

  private handleCompleteClick = () => {
    const questionSnapShot = []

    for (let cat of this.state.categories) {
      questionSnapShot.push([]);
      this.setState({ questionSnapShot })
    }
    let bool = true
    outerLoop: for (let group of this.state.groupedQuestions) {
      for (let question of group) {
        if (question.resp === "No") {
          if (!this.questionHasObservations(question._id)) {
            bool = false;
            break outerLoop;
          }
        }
      }
    }
    if (!bool) {
      // alert("All questions answered 'No' must have corresponding observations.")
      this.handleAlertDialogOpen("questionWithoutObs")
    }
    else if (!this.canCompleteInspection()) {
      // alert("Please ensure that no observations are flagged before completing inspection.")
      this.handleAlertDialogOpen("observationsFlagged")
    } else {
      this.createIncompleteQuestions()
      this.setState({ completeModalOpen: true })
    }
  }

  private handleCompleteInspection = async () => {
    if (navigator.onLine) {
      // Does nothing if the props.inspection doesn't exist
      if (this.props.inspection) {

        const selectedSpaces: Model.Space[] = [];
        const unselectedSpaces: Model.Space[] = [];
        for (let space of this.state.availableSpaces) {
          if (this.state.selectedSpacesById[space._id])
            selectedSpaces.push(space);
          else
            unselectedSpaces.push(space);
        }

        // get all the questions
        const questions: Model.Question[] = [];
        for (let group of this.state.groupedQuestions)
          for (let question of group)
            questions.push(question);

        // get all the observations
        const observations: [Model.Question, Model.NewWorkTask][] = [];
        for (let group of this.state.groupedObservations)
          for (let observation of group)
            observations.push(observation);

        // Likely shows the spinner here
        this.setState({ completingInspection: true, completeModalOpen: false });

        const _multiFloor = this.state.floorValue === 0 ? true : false;
        const name = this.state.name
        const _date = this.state.date ? this.state.date : new Date().toISOString() //in case that update never happens we do it at the end anyway
        const _inspection = { ...this.props.inspection, multifloor: _multiFloor, inspectionname: name, started: true, date: _date } as Model.Inspection
        try {
          Net.completeInspection(_inspection, selectedSpaces,
            unselectedSpaces, questions, observations)
            .then(() => {
              this.setState({ completedInspection: true, completingInspection: false })
              // Commented out to prevent the window from closing. Will add back in in the future.
              // setTimeout(() => {
              //   if (this.props.handleComplete) {
              //     this.props.handleComplete(_inspection);
              //   }
              //   this.setState({ completedInspection: false });
              //   window.close();
                
              // }, 3300);
            })
            .catch((response) => {
              console.log("Net.completeInspection failed!")
              console.log(response)
            })
        } catch (e) {
          this.handleAlertDialogOpen("errorComplete")
        }

      }
    } else {
      this.handleAlertDialogOpen("noNetwork")
    }

  }

  private handleSelectedSpacesChange = (id: any) => (e: any) => {

    const selectedSpacesById = { ...this.state.selectedSpacesById };// take a copy of selected

    if (id in selectedSpacesById) {
      for (let i = 0; i < this.state.groupedObservations.length; i++) {
        const _group = this.state.groupedObservations[i]
        for (let obs of _group) {
          if (obs[1].space.id === id) {
            this.handleAlertDialogOpen("labsRemoved")
            return
          }
        }
      }

      delete selectedSpacesById[id]
    }
    else {
      let space: Model.Space = { _id: 'ddd', name: 'dddd' }
      for (let i = 0; i < this.state.availableSpaces.length; i++) {
        if (this.state.availableSpaces[i]._id === id) {
          space = this.state.availableSpaces[i];
        }
      }
      selectedSpacesById[id] = space;
    }
    this.setState({ selectedSpacesById }, this.saveState);
  }

  private handleStartClick = () => {
    this.setState({ started: true, date: new Date(), tabValue: 1 }, this.saveState);
    if (this.props.inspection) {
      const _inspection = { ...this.props.inspection, date: new Date().toISOString(), started: true, multifloor: this.state.multiFloor } as Model.Inspection
      Net.putInspection(this.props.inspection._id, _inspection)
    }

  }

  private handleCreateClick = async () => {
    if (this.props.buildings && this.props.floors) {
      const building = this.props.buildings[this.state.buildingValue];
      const floor = this.state.floorValue === 0 ? null : this.props.floors[this.state.buildingValue][this.state.floorValue - 1];
      let _multiFloor = false;
      if (this.state.floorValue === 0) {
        _multiFloor = true;
      }
      const _inspection = {
        type: 'Lab Audit',
        comments: null,
        started: false,
        inspectionname: this.state.name,
        multifloor: _multiFloor,
        org: { id: null, value: null },
        space: { id: null, value: null },
        building: { id: building._id, value: building.name },
        date: null, //this is the start date which will be null upon creation until they actually click start
        floor: {
          id: !this.state.multiFloor && floor ? floor._id : null,
          value: !this.state.multiFloor && floor ? floor.name : null
        }
      };
      try {
        const _availableSpaces = await Net.getInspectionSpaces(_inspection);
        if (_availableSpaces.length == 0) {
          // alert("The building/floor you selected have no available spaces.")
          this.handleAlertDialogOpen("noSpacesFound")
        }
        else {
          this.setState({ creatingInspection: true });
          const newInspection = await Net.postInspection(_inspection);
          if (this.props.onCreate) {
            this.props.onCreate(newInspection);
            await this.handleLoad();
          }
          this.setState({ creatingInspection: false }, () => {
            if (window.opener.refreshInspections){
              window.opener.refreshInspections()
            }
          });

        }
      }
      catch (e) {
        if (!navigator.onLine) {
          // alert('No network connection available. Please reconnect and try again.');
          this.handleAlertDialogOpen("noNetwork")
        } else {
          this.handleAlertDialogOpen("errorCreate")
        }
      }
    }
  }
  private canCompleteInspection() {
    if (Object.keys(this.state.selectedSpacesById).length === 0)
      return false;
    for (let group of this.state.groupedObservations) {
      for (let observation of group) {
        if (observation[1].flagged) {
          return false
        }
      }
    }
    return true;
  }
  private handleDismiss = () => {
    this.setState({ completeModalOpen: false })
  }

  private obsSort(a: [Model.Question, Model.NewWorkTask], b: [Model.Question, Model.NewWorkTask]) {
    let aNum = a[0].num;
    let bNum = b[0].num;
    let aId = a[1].id.toLowerCase();
    let bId = b[1].id.toLowerCase();

    if (aNum < bNum) {
      return -1
    } else if (aNum > bNum) {
      return 1
    } else if (aId < bId) {
      return -1
    } else if (aId > bId) {
      return 1
    }

    return 0;
  }

  private handleCreateObservation = (question: Model.Question, task: Model.NewWorkTask, images: any) => {

    let tempArr = []

    if (this.state.groupedObservations.length === 0) {
      for (let category of this.state.categories) {
        tempArr.push([]);
      }
    }
    else {
      tempArr = cloneDeep(this.state.groupedObservations);
    }
    let _index = this.state.categories.findIndex((cat) => {
      if (cat.name === question.cat) {
        return true;
      }
      return false;
    });

    tempArr[_index].push([question, task])
    tempArr[_index].sort(this.obsSort)

    this.setState({
      addObservationQuestion: null, groupedObservations: tempArr, searchValue: '', notificationObservation: true, imageArr: []
    });

    if (images.length > 0 && this.props.inspection) {
      const _id = `inspection-${this.props.inspection._id}-observations-${task.id}-images`;
      const _promise = this.saveStateObsImages(images, _id);
      if (_promise) {
        _promise.then(() => {
          task.images = _id;
          this.saveStateObservations([question, task])
        })
      }
    } else {
      this.saveStateObservations([question, task])
    }
    setTimeout(() => { this.setState({ notificationObservation: false }) }, 1300);

  }
  /*
  Handles Deletion of observation
  @param observationId 
  */
  private updateObservations = (observations: any, observation: any): Promise<any> | undefined => {

    this.setState({ groupedObservations: observations });
    return this.saveStateObservations(observation)
  }

  private deleteObservations = (observations: any, observation: any): Promise<any> | undefined => {
    const count = this.state.observationsCount[observation[0]._id]
    if (count === 1) {
      const questions = cloneDeep(this.state.groupedQuestions)
      for (let group of questions) {
        for (let question of group) {
          if (question._id === observation[0]._id) {
            question.resp = null
          }
        }
      }
      this.setState({ groupedQuestions: questions }, this.saveState)
    }
    this.setState({ groupedObservations: observations })
    return this.deleteSavedStateObservations(observation)
  }

  private getFromDB = async () => {
    if (this.props.inspection) {
      const json = await localforage.getItem(`inspection-${this.props.inspection._id}`) as LabAuditDetails.SavedState;
      return await json
    }
    return null;
  }
  private clearSearch = (event: any) => {
    document.removeEventListener('touchmove', this.stopTouchmove)
    this.setState({ searchValue: '', searchModalOpen: false })
  }

  private getObservationsFromDB = async (categories: any) => {
    if (this.props.inspection) {
      let _id = this.props.inspection._id;
      let json: any = [];
      for (let i = 0; i < categories.length; i++) {
        json.push([]);
      }
      await localforage.iterate((value: any[], key) => {
        if (key.includes(`inspection-${_id}-observations-`) && !key.includes('images')) {
          json[categories.findIndex((cat: any) => { return cat.name === value[0].cat })].push(value);
        }
      })

      return await json;
    }
    return null
  }

  private handleLoad = async () => {

    const savedState: LabAuditDetails.SavedState | null = await this.getFromDB();

    if (this.props.inspection) {

      if (!savedState) {

        const availableSpaces = await Net.getInspectionSpaces(this.props.inspection);
        const groupedQuestions = await Net.getInspectionQuestionsByCategory(this.props.inspection._id)
        const selectedSpacesById = await Net.getInspectionSelectedSpaces(this.props.inspection._id)
        const riskLevels = await Net.getRiskLevels()
        const correctionActions = await Net.getWorkOrderActions()
        const responsiblePersons = await Net.getResponsiblePersons()
        const requestClasses = await Net.getRequestClasses()
        const organizations = await Net.getOrganizations()
        const categories = await Net.getQuestionCategories()

        this.setState({availableSpaces, groupedQuestions, selectedSpacesById,
          riskLevels, correctionActions, responsiblePersons, requestClasses, organizations
          , categories}, 
          this.saveState);

      } else {
        const { groupedQuestions, selectedSpacesById, availableSpaces,
          riskLevels, correctionActions, responsiblePersons,
          requestClasses, organizations, categories, floorValue, started, date, name } = savedState;
        const groupedObservations: any = await this.getObservationsFromDB(categories);
        for (let group of groupedObservations) {
          group.sort(this.obsSort)
        }
        this.setState({
          groupedQuestions, selectedSpacesById, availableSpaces,
          riskLevels, correctionActions, responsiblePersons, requestClasses, organizations,
          categories, floorValue, groupedObservations, started, date, name
        })
      }
    }
    for (let group of this.state.groupedQuestions) {
      for (let question of group) {
        this.totalQuestionCount++;
      }
    }

  }

  private handleTabChange = (event: React.ChangeEvent<{}>, tab: number) => {
    const _questionScrollDiv = document.getElementById("questionScrollDiv");
    if (_questionScrollDiv) {
      _questionScrollDiv.removeEventListener("touchstart", this.touchstartEvent)
      _questionScrollDiv.removeEventListener("touchmove", this.touchmoveEvent)
    }
    const _spaceScrollDiv = document.getElementById("spaceScrollDiv");
    if (_spaceScrollDiv) {
      _spaceScrollDiv.removeEventListener("touchstart", this.touchstartEvent)
      _spaceScrollDiv.removeEventListener("touchmove", this.touchmoveEvent)
    }

    this.setState({ tabValue: tab }, () => {
      if (tab === 1) {
        if (_spaceScrollDiv) {
          _spaceScrollDiv.addEventListener("touchstart", this.touchstartEvent)
          _spaceScrollDiv.addEventListener("touchmove", this.touchmoveEvent)
        }
      } else if (tab === 2) {
        if (_questionScrollDiv) {
          _questionScrollDiv.addEventListener("touchstart", this.touchstartEvent)
          _questionScrollDiv.addEventListener("touchmove", this.touchmoveEvent)
        }
      }
    });
  }

  private handleCategoryTabChange = (event: React.ChangeEvent<{}>, tab: number) => {
    if (this.state.searchValue !== '') {
      this.setState({ searchValue: '' });
    }
    this.setState({ questionCategoryTab: tab });
  }


  private goToNextTab = (event: React.MouseEvent<HTMLElement>) => {

    this.setState({ tabValue: 2 });
  }

  private stopTouchmove = (e: any) => {
    e.preventDefault();
  }

  private handleSearchModalClose = () => {
    document.removeEventListener('touchmove', this.stopTouchmove)
    this.setState({ searchModalOpen: false })
  }

  private handleSearchModalOpen = () => {
    document.addEventListener('touchmove', this.stopTouchmove, { passive: false })
    this.setState({ searchModalOpen: true });
  }

  private handleSearchInput = (event: any) => {
    if (this.state.questionCategoryTab !== 0) {
      this.setState({ questionCategoryTab: 0 });
    }


    this.setState({ searchValue: event.target.value })
  }



  private handleSearchEnter = (event: any) => {
    if (event.keyCode === 13 || event.keyCode === 27) {
      this.handleSearchModalClose();
    }
  }

  private handleSelectChoiceBuilding = (event: any) => {
    if (this.props.buildings && this.props.floors) {
      const _displayedFloors = [{ name: "Multifloor", _id: -1 }, ...this.props.floors[event.target.value]] as Model.Floor[];
      const _floorOpen = this.state.buildingValue === event.target.value ? false : true;
      const _floorValue = this.state.buildingValue === event.target.value ? this.state.floorValue : -1;
      this.setState({ buildingValue: event.target.value, floorValue: _floorValue, displayedFloors: _displayedFloors, floorSelectOpen: _floorOpen })
    }
  }

  private handleSelectChoiceFloor = (event: any) => {
    if (this.props.buildings && this.props.floors) {

      this.setState({ floorValue: event.target.value, floorSelectOpen: false })
    }
  }

  private handleFloorClose = (event: any) => {
    this.setState({ floorSelectOpen: false })
  }

  private handleFloorOpen = (event: any) => {
    this.setState({ floorSelectOpen: true })
  }

  private handleNameChange = (event: any) => {
    this.setState({ name: event.target.value }, this.saveState);
  }



  private takePicture = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target;
    const reader = new FileReader();
    if (input.files && input.files[0]) {

      this.resizeImage({ file: input.files[0], maxSize: 500 })
        .then((res: any) => {
          reader.onloadend = (e: any) => {
            if (this.pictureInput && this.image && e.target) {
              const _imageArr = [...this.state.imageArr];
              if (_imageArr.length === 3 && input.files && input.files[0]) {
                _imageArr[2] = { imageURL: e.target.result };
              } else if (input.files && input.files[0]) {
                _imageArr[_imageArr.length] = { imageURL: e.target.result };
              }
              this.setState({ imageArr: _imageArr })
              this.pictureInput.value = '';
            }
          }
          reader.readAsArrayBuffer(res);
        })


    }
  }
  private cameraClick = (event: React.MouseEvent<SVGElement>) => {
    if (this.pictureInput) {
      if (this.state.imageArr.length >= 3) {
        this.handleDialogOpen('overrideImg')()
      } else {
        this.pictureInput.click();
      }
    }
  }

  private confirmTakePicture = () => {
    if (this.pictureInput) {
      this.pictureInput.click();
    }
  }

  private handleMultiFloorChange = async () => {
    if (navigator.onLine) {
      if (this.props.inspection && this.props.handleMultiFloorChange) {
        let spaces = await this.props.handleMultiFloorChange(this.props.inspection)

        const _availableSpaces = [...spaces] as Model.Lab[];
        this.setState({ availableSpaces: _availableSpaces, floorValue: 0 }, this.saveState);
      }
    }
    else {
      this.handleAlertDialogOpen("floorError");
    }
  }

  private getToggleSectionStyle = (questions: Model.Question[]): React.CSSProperties => {
    for (let question of questions)
      if (question.resp === null || question.resp === "No" && !this.state.observationsCount[question._id]) {
        return { color: 'rgb(106,156,224)', fill: 'rgb(106,156,224)' };
      }
    return {};
  }

  private getToggleAllStyle = (allQuestions: Model.Question[][]): React.CSSProperties => {
    for (let questions of allQuestions)
      for (let question of questions)
        if (question.resp === null || question.resp === "No" && !this.state.observationsCount[question._id]) {
          return { color: 'rgb(106,156,224)', fill: 'rgb(106,156,224)' };
        }
    return {};
  }

  private deleteImage = () => {
    const imageArr = this.state.imageArr.slice()
    imageArr.splice(this.state.deleteIndex, 1);

    this.setState({ imageArr, deleteIndex: -1 });
  }

  private imageClick = (e: any) => {
    document.addEventListener('touchmove', this.stopTouchmove, { passive: false })
    this.setState({ previewUrl: e.target.src, previewImage: true })
  }

  private previewClose = () => {
    document.removeEventListener('touchmove', this.stopTouchmove)
    this.setState({ previewUrl: '', previewImage: false })
  }

  private obsPreviewImage = (url: any) => {
    document.addEventListener('touchmove', this.stopTouchmove, { passive: false })
    this.setState({ previewUrl: url, previewImage: true })
  }

  private handleDialogClose = () => {
    this.setState({ openDialog: false });
  }
  private handleAlertDialogOpen = (type: string) => {
    let msg = "";

    if (type === "questionChangeObs") {
      msg = "Please delete observations associated to this question before changing the answer."
    }
    else if (type === "questionWithoutObs") {
      msg = "All questions answered 'No' must have corresponding observations."
    }
    else if (type === "labsRemoved") {
      msg = "Labs cannot be removed until all corresponding Observations are deleted."
    }
    else if (type === "observationsFlagged") {
      msg = "Please ensure no observations are flagged before completing the inspection."
    }
    else if (type === "noSpacesFound") {
      msg = "The floor(s) you have selected has no auditable spaces."
    }
    else if (type === "noNetwork") {
      msg = "No network connection available. Please reconnect and try again."
    }
    else if (type === "errorComplete") {
      msg = "An error has occurred while completing the inspection. Please try again."
    }
    else if (type === "errorCreate") {
      msg = "An error has occurred while creating the inspection. Please try again."
    }
    else if (type === "floorError") {
      msg = "Network unavailable, floors not updated."
    }
    this.setState({ alertDialog: true, alertMessage: msg })
  }

  private handleDialogOpen = (type: string, index?: number) => () => {
    let msg = "";

    switch (type) {
      case 'deleteImg':
        msg = "Are you sure you want to delete the image ?"
        break;
      case 'retire':
        msg = "Are you sure you want to retire the inspection ?"
        break;
      case 'cancelCreate':
        msg = "Are you sure you want to cancel this observation ?"
        break;
      case 'overrideImg':
        msg = "Your third image will be overwritten. Are you sure you want to continue?";
        break;
    }
    let stateObj = {}
    if (index) {
      stateObj = { deleteIndex: index }
    }
    this.setState({ ...stateObj, openDialog: true, dialogType: type, dialogMessage: msg });
  }
  private getImages = () => {
    let _imageArr: any = [];
    for (let i = 0; i < 3; i++) {
      if (this.state.imageArr[i]) {
        const dataView = new DataView(this.state.imageArr[i].imageURL);
        const blob = new Blob([dataView], { type: 'image/jpeg' });
        const imageUrl = URL.createObjectURL(blob);

        _imageArr.push(<div
          key={i}

          style={{ display: 'inline-flex', width: 125, height: 125, position: 'relative' }} >
          <img onClick={this.imageClick} src={imageUrl} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
          <IconButton disableRipple style={{ color: 'red', position: 'absolute', right: '0' }} onClick={this.handleDialogOpen('deleteImg', i)}>{this.deleteIcon}</IconButton>
        </div>);
      } else {
        _imageArr.push(<div key={i}>
          {this.imagePlaceHolder}
        </div>)
      }
    }
    return _imageArr;
  }
  private routeToObservations = (question: Model.Question) => {
    const questionCatIndex = this.state.categories.findIndex((category) => category.name === question.cat) + 1
    this.setState({ completeModalOpen: false, tabValue: 2, questionCategoryTab: questionCatIndex })
  }
  private handleDialogCloseClick = () => {
    switch (this.state.dialogType) {
      case 'deleteImg':
        this.deleteImage()
        break;
      case 'retire':
        this.handleRetireInspection()
        break;
      case 'cancelCreate':
        this.handleModalClose()
        break;
      case 'overrideImg':
        this.confirmTakePicture()

    }
    this.setState({ openDialog: false })
  }
  private handleAlertDialogClose = () => {
    this.setState({ alertDialog: false })
  }

  render() {
    const date = this.state.date;
    const selectedSpacesById = this.state.selectedSpacesById;
    const selectedSpaces = Object.keys(selectedSpacesById).map(
      key => selectedSpacesById[key]);

    return <Loader load={this.handleLoad}>
      <div id="tabsContainer" style={{ backgroundColor: 'rgb(237,237,237)', height: 'calc(100vh - 65px)', overflow: 'hidden' }}>
        <Tabs indicatorColor="primary" fullWidth onChange={this.handleTabChange} value={this.state.tabValue}>
          <Tab value={0} label="Inspection Details" />


          <Tab disabled={this.props.params === "newInspection" || this.state.started === false} value={1} label="Labs" />
          <Tab disabled={Object.keys(this.state.selectedSpacesById).length == 0 || this.props.params === "newInspection"} value={2} label="Inspection Findings" />
          <Tab disabled={Object.keys(this.state.selectedSpacesById).length == 0 || this.props.params === "newInspection"} value={3} label="Observations" />


        </Tabs>
        <TabContainer active={this.state.tabValue === 0}>
          <div style={{ backgroundColor: 'rgb(237 ,237 ,237)', padding: 20 }} id="formContainer">
            <span>Start Date:&nbsp;{date && date.toISOString().split('T')[0]}</span>
            <FormControl fullWidth style={{ marginBottom: 20, marginTop: 5 }}>
              <InputLabel style={{ zIndex: 1 }} htmlFor="nameInput">Enter Inspection Name</InputLabel>
              <Input inputProps={{ id: 'nameInput' }} value={this.state.name} onChange={this.handleNameChange} style={{ textAlign: 'center', background: 'white', border: 'lightgrey 1px solid', borderRadius: 4, paddingLeft: 5, paddingRight: 5 }}></Input>
            </FormControl>
            {/* <FormControl fullWidth style={{ marginBottom: 20 }}>
              <InputLabel htmlFor="dateInput">Inspection Date</InputLabel>
              <Input disabled type="date" id="dateInput" value={date.toISOString().split('T')[0]} style={{ textAlign: 'center', background: 'white', border: 'rgba(0, 0, 0, 0.54)' }}></Input>
            </FormControl> */}
            <FormControl fullWidth style={{ marginBottom: 20 }}>
              <InputLabel htmlFor="buildingInput">Select Building</InputLabel>
              <SelectMaterial
                MenuProps={{ style: { maxHeight: 'calc(100vh - 247px)' }, anchorOrigin: { vertical: 'bottom', horizontal: 'left' }, anchorPosition: { top: 0, left: 0 }, getContentAnchorEl: undefined }}
                id="buildingInput"
                style={{ boxSizing: 'border-box', display: 'block', width: '100%', background: 'white', border: 'lightgrey 1px solid', borderRadius: 4, paddingLeft: 5, paddingRight: 5 }}
                disabled={this.props.params !== "newInspection"}
                name="buildingSelect"
                onChange={this.handleSelectChoiceBuilding}
                type="select" value={this.state.buildingValue}>
                {this.props.buildings && this.props.buildings.map((building, index) => {
                  return <MenuItem key={index} value={index}>{building.name}</MenuItem>
                })}
              </SelectMaterial>
            </FormControl>
            <FormControl fullWidth style={{ marginBottom: 20 }}>
              <InputLabel htmlFor="floorInput">Select Floor</InputLabel>
              <SelectMaterial
                onOpen={this.handleFloorOpen}
                onClose={this.handleFloorClose}
                open={this.state.floorSelectOpen}
                MenuProps={{ style: { maxHeight: 'calc(100vh - 315px)' }, anchorOrigin: { vertical: 'bottom', horizontal: 'left' }, getContentAnchorEl: undefined }}
                id="floorInput"
                style={{ boxSizing: 'border-box', display: 'block', width: '100%', background: 'white', border: 'lightgrey 1px solid', borderRadius: 4, paddingLeft: 5, paddingRight: 5 }}
                disabled={this.props.params !== "newInspection" || this.state.buildingValue === -1}
                name="floorSelect"
                onChange={this.handleSelectChoiceFloor}
                type="select"
                value={this.state.floorValue}>
                {this.props.floors && this.state.buildingValue > -1 && this.state.displayedFloors.map((floor, index) => {
                  return <MenuItem key={index} value={index}>{floor.name}</MenuItem>
                })}
              </SelectMaterial>
            </FormControl>

            <ButtonMaterial disableRipple style={this.props.inspection ? { display: 'none' } : { display: 'block', margin: '0 auto', width: '80%', color: 'white' }} disabled={this.state.buildingValue === -1 || this.state.floorValue === -1} variant="raised" color="secondary" onClick={this.handleCreateClick}>Create Inspection</ButtonMaterial>
            {!this.state.started && <ButtonMaterial disableRipple style={this.props.inspection ? { display: 'block', margin: '0 auto', width: '80%', color: 'white' } : { display: 'none' }} disabled={this.state.buildingValue === -1 || this.state.floorValue === -1} color="secondary" variant="raised" onClick={this.handleStartClick}>Start Inspection</ButtonMaterial>}
            <ButtonMaterial disableRipple style={!this.state.started ? { display: 'none' } : { display: 'block', margin: '0 auto', width: '80%', color: 'white' }} disabled={!(this.props.inspection && this.state.floorValue !== 0)} color="secondary" variant="raised" onClick={this.handleMultiFloorChange}>Switch to Multi-Floor</ButtonMaterial>

          </div>

          <div style={{ textAlign: 'right' }}>
            {/* <button onClick={this.handleFillSurvey}
            >Fill Survey</button>&nbsp;&nbsp;&nbsp; */}
            {this.props.params !== "newInspection" && <button onClick={this.handleDialogOpen('retire')}
            >Retire Inspection</button>}&nbsp;&nbsp;&nbsp;
                {/* <button disabled={!this.canCompleteInspection()}
              onClick={this.handleCompleteInspection}>Complete Inspection</button> */}
          </div>
        </TabContainer>
        <TabContainer active={this.state.tabValue === 1}>
          <Typography style={{ padding: 10 }} align="center" variant="subheading">Please select one or more labs</Typography>
          <ButtonMaterial disableRipple style={!this.state.started ? { display: 'none' } : { display: 'block', margin: '6px auto', width: '80%', color: 'white' }} disabled={!(this.props.inspection && this.state.floorValue !== 0)} color="secondary" variant="raised" onClick={this.handleMultiFloorChange}>Switch to Multi-Floor</ButtonMaterial>

          <div id="spaceScrollDiv" style={{ width: '80%', margin: '20px auto', maxHeight: '50vh', overflow: 'scroll', WebkitOverflowScrolling: 'touch', border: '1px solid grey' }}>

            <Table style={{ backgroundColor: 'white' }}>
              <TableBody>
                {this.state.availableSpaces.map((space, index) => {
                  let selectedRow = false;
                  for (let selectedSpace in this.state.selectedSpacesById) {
                    if (selectedSpace === space._id) {
                      selectedRow = true
                    }
                  }
                  return <TableRow style={{ cursor: 'pointer' }} selected={selectedRow} onClick={this.handleSelectedSpacesChange(space._id)} key={index}>
                    <TableCell style={{ textAlign: 'center', border: '1px grey solid' }}>{space.name}</TableCell>
                  </TableRow>
                })}

              </TableBody>
            </Table>
          </div>
          <div style={{ textAlign: 'center', padding: 15 }}>
            <ButtonMaterial disableRipple disabled={Object.keys(this.state.selectedSpacesById).length == 0} style={{ textAlign: 'center', width: '80%', color: 'white' }} variant="raised" color="secondary" onClick={this.goToNextTab}>Continue</ButtonMaterial>
          </div>
        </TabContainer>



        <TabContainer active={this.state.tabValue === 2}>
          <TabContainer active={this.state.addObservationQuestion ? false : true}>
            <img style={{ visibility: 'hidden', position: 'absolute', height: 1, width: 1, top: -10 }} ref={e => this.image = e} />
            <div style={{ display: 'flex' }}>
              <div style={{ display: 'flex', boxSizing: 'border-box', margin: '15px 0', backgroundColor: 'rgb(237, 237 ,237)', height: 150, width: '100%' }} id="pictureSearch">
                <div style={{ alignContent: 'center', alignSelf: 'center', boxSizing: 'border-box', width: '25%', height: 150, display: 'inline-flex' }}>
                  <svg style={{ display: 'block', margin: 'auto', borderRadius: 20, boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)' }} onClick={this.cameraClick} width="150px" height="150px" viewBox="0 0 157 157" version="1.1">
                    <title>Take/Upload Picture</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Group-9">
                        <g id="Group-2-Copy-9" fill="#FFFFFF">
                          <rect id="Rectangle" x="0" y="0" width="157" height="157" rx="15"></rect>
                        </g>
                        <g id="camera-copy-8" transform="translate(38.000000, 45.000000)" stroke="#314474" strokeLinecap="round" strokeLinejoin="round" strokeWidth="6">
                          <path d="M82,59.5555556 C82,63.6670087 78.6624863,67 74.5454545,67 L7.45454545,67 C3.33751368,67 8.276208e-16,63.6670087 0,59.5555556 L0,18.6111111 C-4.138104e-16,14.499658 3.33751368,11.1666667 7.45454545,11.1666667 L22.3636364,11.1666667 L29.8181818,0 L52.1818182,0 L59.6363636,11.1666667 L74.5454545,11.1666667 C78.6624863,11.1666667 82,14.499658 82,18.6111111 L82,59.5555556 Z" id="Shape"></path>
                          <circle id="Oval" cx="41" cy="37" r="15"></circle>
                        </g>
                      </g>
                    </g>
                  </svg>
                  <input style={{ visibility: 'hidden', position: 'absolute' }} onChange={this.takePicture} ref={e => this.pictureInput = e} type="file" name="myImage" accept="image/x-png,image/gif,image/jpeg,image/png,image/jpg" />

                </div>
                <div style={{ width: '50%', display: 'inline-flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  {this.getImages()}
                </div>
                <div style={{ alignContent: 'center', alignSelf: 'center', boxSizing: 'border-box', width: '25%', height: 150, display: 'inline-flex' }}>
                  <svg style={{ display: 'block', margin: 'auto', borderRadius: 20, boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)' }} onClick={this.handleSearchModalOpen} width="150px" height="150px" viewBox="0 0 157 157" version="1.1">
                    <title>Search Questions</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Group-10">
                        <g id="Group-Copy" fill="#FFFFFF">
                          <rect id="Rectangle-Copy" x="0" y="0" width="157" height="157" rx="15"></rect>
                        </g>
                        <g id="search" transform="translate(47.000000, 45.000000)" stroke="#314474" strokeWidth="6">
                          <circle id="Oval" cx="28" cy="28" r="28"></circle>
                          <path d="M68,68 L48,48" id="Shape" strokeLinecap="round"></path>
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>

              </div>
            </div>
            <Tabs indicatorColor="primary" className="questionCategoryTab" onChange={this.handleCategoryTabChange} scrollable scrollButtons="auto" value={this.state.questionCategoryTab}>
              <Tab style={this.getToggleAllStyle(this.state.groupedQuestions)} value={0} label="All Questions"></Tab>
              {this.state.categories.map((category, catIndex) => {
                return <Tab style={this.getToggleSectionStyle(this.state.groupedQuestions[catIndex])} key={catIndex} value={catIndex + 1} label={category.name}></Tab>
              })}
            </Tabs>

            <div id="questionScrollDiv" style={{ border: '1px solid grey', maxHeight: 'calc(100vh - 381px)', backgroundColor: 'white', overflow: 'scroll', margin: 20, WebkitOverflowScrolling: 'touch' }}>
              {this.state.groupedQuestions.map((questions, i) => {
                return this.state.questionCategoryTab === 0 ? questions.filter(q => q.text.toLowerCase().includes(this.state.searchValue.toLowerCase())).map((q, j) =>
                  <Question category={q.cat} key={j} onChange={this.handleQuestionChange}
                    onAddObservation={this.handleAddObservation}
                    question={q}
                    observationCount={this.state.observationsCount[q._id] || 0} />)
                  :
                  this.state.questionCategoryTab === i + 1 ? questions.map((q, j) =>
                    <Question key={j} onChange={this.handleQuestionChange}
                      onAddObservation={this.handleAddObservation}
                      question={q}
                      observationCount={this.state.observationsCount[q._id]} />) : null;
              })}
            </div>
          </TabContainer>
          <TabContainer active={this.state.addObservationQuestion ? true : false}>
            {this.state.addObservationQuestion &&
              <div style={{
                padding: '5px 15px',
                width: '100vw',
                height: '100vh',
                position: 'fixed',
                top: '0px',
                left: '0',
                background: 'white',
                boxSizing: 'border-box',
                overflowY: 'scroll',
                overflowX: 'hidden'
              }}>
                <ObservationCreator
                  question={this.state.addObservationQuestion}
                  riskLevels={this.state.riskLevels}
                  correctionActions={this.state.correctionActions}
                  responsiblePersons={this.state.responsiblePersons}
                  labs={selectedSpaces}
                  requestClasses={this.state.requestClasses}
                  organizations={this.state.organizations}
                  onCancel={this.handleDialogOpen("cancelCreate")}
                  onCreate={this.handleCreateObservation}
                  images={this.state.imageArr}
                  urls={this.urls}
                  imageClick={this.imageClick}
                />
              </div>}
          </TabContainer>
        </TabContainer>


        <TabContainer active={this.state.tabValue === 3}>
          <ObservationsTab
            groupedQuestions={this.state.groupedQuestions}
            selectedSpacesById={this.state.selectedSpacesById}
            riskLevels={this.state.riskLevels}
            correctionActions={this.state.correctionActions}
            responsiblePersons={this.state.responsiblePersons}
            requestClasses={this.state.requestClasses}
            organizations={this.state.organizations}
            groupedObservations={this.state.groupedObservations}
            categories={this.state.categories}
            labs={selectedSpaces}
            updateObservations={this.updateObservations}
            deleteObservations={this.deleteObservations}
            urls={this.urls}
            inspectionId={this.props.inspection ? this.props.inspection._id : -1}
            obsPreviewImage={this.obsPreviewImage}
          ></ObservationsTab>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '68px' }}>
            <ButtonMaterial disableRipple style={{ width: '80%', backgroundColor: 'rgb(99,177,219)', color: 'white' }} onClick={this.handleCompleteClick} variant="raised" >Complete Inspection</ButtonMaterial>
          </div>
        </TabContainer>
      </div>
      <Modal zIndex={12} isOpen={this.state.completingInspection}>
        <div style={{ flexDirection: 'column', display: 'flex', justifyContent: 'center', alignItems: 'center', padding: 5, left: '50%', top: '50%', transform: 'translate(-50%,-50%)', position: 'absolute', backgroundColor: 'white', width: '80%', height: '100px', textAlign: 'center' }}>
          <Typography style={{ marginBottom: '10px' }} variant="title">Completing Inspection, Please Wait...</Typography>
          <CircularProgress />
        </div>
      </Modal>
      <Modal zIndex={9} isOpen={this.state.notificationObservation}>
        <ObservationNotification></ObservationNotification>
      </Modal>
      <Modal zIndex={11} isOpen={this.state.completedInspection}>
        <CompletionNotification></CompletionNotification>
      </Modal>
      <ModalMaterial
        onClose={this.handleSearchModalClose}
        open={this.state.searchModalOpen}>
        <div style={{ padding: 5, left: '50%', top: '50%', transform: 'translate(-50%,-50%)', position: 'absolute', backgroundColor: 'white', width: '80%' }} id="searchModalContainer">
          <TextField value={this.state.searchValue} onChange={this.handleSearchInput} inputProps={{ onKeyDown: this.handleSearchEnter }} inputRef={(e) => { e && e.focus() }} InputProps={{ disableUnderline: true }} style={{ width: '100%' }} placeholder="Search for inspector Findings Questions"></TextField>
          {this.state.searchValue && this.state.searchValue !== "" && <IconButton disableRipple onClick={this.clearSearch} style={{ position: 'absolute', top: 5, right: 5, height: 30, width: 30 }}><CloseIcon></CloseIcon></IconButton>}
        </div>
      </ModalMaterial>
      <ModalMaterial open={this.state.creatingInspection}>
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: 5, left: '50%', top: '50%', transform: 'translate(-50%,-50%)', position: 'absolute', backgroundColor: 'white', width: '80%', height: '100px', textAlign: 'center' }}>
          <Typography style={{ marginBottom: '10px' }} variant="title">Creating Inspection, Please Wait...</Typography>
          <CircularProgress />
        </div>
      </ModalMaterial>
      <ModalMaterial
        open={this.state.previewImage}
        onClose={this.previewClose}>
        <div style={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%,-50%)' }}>
          <img src={this.state.previewUrl} />
        </div>
      </ModalMaterial>
      <Modal zIndex={9} isOpen={this.state.completeModalOpen}>
        <div>
          <div style={{ marginTop: 20, display: 'flex', justifyContent: 'flex-end' }}>
            <IconButton disableRipple onClick={this.handleDismiss}><CloseIcon></CloseIcon></IconButton>
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'row', height: 'calc(100vh - 88px)' }}>
            {!this.state.answersComplete ?
              <div style={{ flexGrow: 1 }}>
                <Typography style={{ textAlign: 'center' }} variant="title">You have completed {this.state.answeredCount} out of {this.totalQuestionCount} inspection questions</Typography>
                <Typography style={{ marginBottom: '10px', textAlign: 'center' }} variant="subheading">Please answer the questions below.</Typography>
                <div style={{ overflow: 'scroll', maxHeight: 'calc(100vh - 190px)', border: '1px solid grey', WebkitOverflowScrolling: 'touch' }}> {
                  this.state.questionSnapShot.map(group => group.map((q: any, i: any) => {
                    return <Question
                      onRoute={this.routeToObservations}
                      category={q.cat} key={i}
                      onChange={this.handleQuestionChange}
                      question={q}
                      observationCount={this.state.observationsCount[q._id] || 0} />
                  }))
                }</div>
              </div> :
              <div>
                <p style={{ fontSize: '2em', textAlign: 'center' }}>Please confirm that you are ready to complete the inspection.</p>
                <div style={{ height: '68px', width: '80%', margin: '0 auto' }}>
                  <ButtonMaterial disableRipple style={{ width: '100%', color: 'white' }} color="secondary" onClick={this.handleCompleteInspection} variant="raised">Complete Inspection</ButtonMaterial>
                </div>
              </div>
            }

          </div>
        </div>

      </Modal>
      <Dialog open={this.state.openDialog} onClose={this.handleDialogClose}>
        <DialogTitle>
          {this.state.dialogMessage}
        </DialogTitle>
        <DialogActions>
          <ButtonMaterial variant="outlined" onClick={this.handleDialogClose} disableRipple>Cancel</ButtonMaterial>
          <ButtonMaterial variant="raised" onClick={this.handleDialogCloseClick} disableRipple>Confirm</ButtonMaterial>
        </DialogActions>
      </Dialog>
      <Dialog open={this.state.alertDialog} onClose={this.handleAlertDialogClose}>
        <DialogTitle>
          {this.state.alertMessage}
        </DialogTitle>
        <DialogActions>
          <ButtonMaterial variant="raised" onClick={this.handleAlertDialogClose} disableRipple>Ok</ButtonMaterial>
        </DialogActions>
      </Dialog>
    </Loader >;
  }
}
export default withRouter<LabAuditDetails.Props>(LabAuditDetails) 