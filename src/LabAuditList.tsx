import * as React from 'react';
import * as Model from './Model';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import LabAuditListItem from './LabAuditListItem'
import ButtonMaterial from '@material-ui/core/Button';




module LabAuditList {
  export interface Props extends RouteComponentProps<any> {
    inspections: Model.Inspection[];
    onNewInspection?: () => void;
  }
}

class LabAuditList extends React.PureComponent<LabAuditList.Props> {
  private handleClick = (id: string) => {
    this.props.history.push(`/${id}`);
  }
  private lastY = 0;
  componentDidMount() {
    const _inspectionScrollDiv = document.getElementById("inspectionScrollDiv");
    if (_inspectionScrollDiv) {
      _inspectionScrollDiv.addEventListener("touchstart", this.touchstartEvent)
      _inspectionScrollDiv.addEventListener("touchmove", this.touchmoveEvent)
    }
  }
  componentWillUnmount() {
    const _inspectionScrollDiv = document.getElementById("inspectionScrollDiv");
    if (_inspectionScrollDiv) {
      _inspectionScrollDiv.removeEventListener("touchstart", this.touchstartEvent)
      _inspectionScrollDiv.removeEventListener("touchmove", this.touchmoveEvent)
    }
  }

  private touchstartEvent = (e: any) => {
    this.lastY = e.touches[0].clientY;
  }
  private touchmoveEvent = (e: any) => {
    const top = e.touches[0].clientY;
    const scrollTop = e.currentTarget.scrollTop;
    const direction = (this.lastY - top < 0 ? "up" : "down")

    if (scrollTop == 0 && direction === "up") {
      e.preventDefault();
    } else if (scrollTop >= (e.currentTarget.scrollHeight - e.currentTarget.offsetHeight) && direction === "down") {
      e.preventDefault();
    }
  }
  render() {

    if (this.props.inspections.length === 0) {
      return <div style={{ paddingTop: '44px', display: 'flex' }}>
        <div style={{ margin: 'auto' }}>
          No Inspections
          <div style={{ display: 'flex', marginTop: '10px' }}>
            <button style={{ margin: 'auto' }}
              onClick={this.props.onNewInspection}>Create</button>
          </div>
        </div>
      </div>;
    }


    const linkStyle: React.CSSProperties = {

      color: 'white'
    };

    return <div id="inspectionScrollDiv" style={{ overflow: 'scroll', height: 'calc(100vh - 64px)', WebkitOverflowScrolling: 'touch' }}>
      {this.props.inspections.map((inspection, i) =>
        <div key={i} style={{
          textAlign: 'center',
          padding: '10px',
          borderBottom: '1px solid #ddd'
        }}>
        <LabAuditListItem inspection={inspection}></LabAuditListItem>
        </div >
      )}
    </div>;
  }
}
export default withRouter<LabAuditList.Props>(LabAuditList);
