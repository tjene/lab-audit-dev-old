import * as React from 'react';
import * as Model from './Model';
import 'react-select/dist/react-select.css';
import  '../src/style.css';
import ButtonMaterial from  '@material-ui/core/Button';


module Question {
    export interface Props {
      question: Model.Question;
      category?:string;
      observationCount: number;
      style?:React.CSSProperties;
      onAddObservation?: (question: Model.Question) => void;
      onChange?: (question: Model.Question) => void;
      onRoute?: (question: Model.Question) => void;
    }
  }

class Question extends React.PureComponent<Question.Props> {
    private static readonly style: React.CSSProperties = {
      padding: '10px',
      border:'1px solid grey'
    };
    private handleAddObservation = () => {
      if (this.props.onAddObservation)
        this.props.onAddObservation(this.props.question);
    }
    // 
    private handleBtnClick = (e:React.MouseEvent<any>) => {
      e.stopPropagation();
      if (this.props.onChange){
        const target= e.target as HTMLElement;
        if (this.props.question.resp === target.textContent){
          this.props.onChange({ ...this.props.question, resp: null});
        } else {
          this.props.onChange({ ...this.props.question, resp: target.textContent});
        }
      }
    }
    private handleButtonClick = () => {
      if (this.props.onRoute){
        this.props.onRoute(this.props.question);
      } else {
        this.handleAddObservation();
      }
      
    }
    render() {
      const options = [
        { label: 'Yes', value: 'Yes' },
        { label:  'No', value:  'No' },
      ];
      if (this.props.question.type.value === 'Yes/No/NA')
        options.push({ label: 'N/A', value: 'N/A' });
        const headerStyle = this.props.question.resp === null || this.props.question.resp === "No" && !this.props.observationCount ? {
        color: 'rgb(106,156,224)'
      } : {};
  
      return (
        <div style={Question.style}>
          <div style={{display:'flex', alignItems:'center'}}>
            <div style={{width:'50%', display:'inline-block', paddingRight: '5px', boxSizing: 'border-box'}}>{this.props.category &&<div style={{ display:'block'}}><h6 style={{ ...headerStyle, margin: '0', marginBottom: '4px',display:'inline' }}>{this.props.question.cat}</h6></div> }<h4 style={{ ...headerStyle, margin: '0', marginBottom: '4px',display:'inline' }}>{this.props.question.text}</h4></div>
            <div style={{width:'50%', display:'inline-flex', paddingLeft: '5px', boxSizing: 'border-box' }}>{options.map((option,optionIndex)=>{
              return <ButtonMaterial disableRipple onClick={this.handleBtnClick} style={this.props.question.resp === option.value ? {backgroundColor:'rgb(99,177,219)',margin:6, flexGrow:1,color:'white'}: {backgroundColor:'white',margin:6, flexGrow:1}} key={optionIndex} variant="raised" >{option.label}</ButtonMaterial>
            })}
            </div>
          </div>
          {
            this.props.question.resp === 'No' &&
            <div style={{ marginTop: '10px', borderTop: '1px solid lightgrey', paddingTop: '10px', display:'flex' }}>
              <div style={{...headerStyle, display:'inline-flex', flexGrow:1, maxWidth:'50%'}}><p># of Observations Created: {this.props.observationCount ? this.props.observationCount : 0}</p></div>
              <ButtonMaterial disableRipple color="primary" variant="raised" onClick={this.handleButtonClick} style={{padding:'10 0', boxSizing:'border-box', display:'inline-flex', flexGrow:1, maxWidth:'calc(50% - 17px)', left:'11px', position:'relative', paddingLeft:10}}>
                Create New Observation
              </ButtonMaterial>
            </div>
          }
        </div>
      );
    }
  }
  export default Question;