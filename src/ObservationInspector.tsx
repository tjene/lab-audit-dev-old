import * as React from 'react';
import * as Model from './Model';
import * as _localforage from 'localforage';
import Select from 'react-select';
import '../src/style.css';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import DownIcon from '@material-ui/icons/ExpandMore';
import UpIcon from '@material-ui/icons/ExpandLess';
import ButtonMaterial from '@material-ui/core/Button';
import 'react-widgets/dist/css/react-widgets.css';
import { isEqual } from 'lodash';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

const localforage = (_localforage as any).default as typeof _localforage; //localStorage delcaration

module ObservationInspector {
    export interface Props {
        observation: Model.NewWorkTask;
        riskLevels: Model.RiskLevel[];
        correctionActions: Model.WorkOrderAction[];
        responsiblePersons: Model.ResponsiblePerson[];
        labs: Model.Space[];
        requestClasses: Model.RequestClass[];
        organizations: Model.Organization[];
        question: Model.Question
        onChange: (id: number, catId: number, question: Model.Question, observation: Model.NewWorkTask) => void;
        handleFlaggedChange: (id: number, catId: number, question: any) => any;
        handleOrganizationChange: (id: number, catId: number, question: any) => any;
        handleLabChange: (id: number, catId: number, question: any) => any;
        handleRiskChange: (id: number, catId: number, question: any) => any;
        handleCorrectiveActionChange: (id: number, catId: number, question: any) => any;
        handleResponsiblePersonChange: (id: number, catId: number, question: any) => any;
        handleRequestClassChange: (id: number, catId: number, question: any) => any;
        handleBenchChange: (id: number, catId: number, question: any) => any;
        handleDescriptionSelect: (value: any, id: number, catId: number, question: any) => any;
        handleDescriptionChange: (event: any, id: number, catId: number, question: any) => any;
        handleAuditedDepartmentsChange: (id: number, catId: number, question: any) => any;
        handleImagesChange: (images: any, id: number, catId: number, question: any, task: any) => any;
        handleImageDelete: (index: any, id: number, catId: number, question: any, task: any) => any;
        handleObservationContinue: (id: number, catId: number, question: any, task: any) => any;
        onDelete: (id: number, catId: number) => void
        onCopy: (id: number, question: Model.Question, observation: Model.NewWorkTask) => void;
        getImages: (images: string) => any;
        id: number;
        catId: number;
        auditedDepartments: Model.Organization[];
        urls: any;
        imageClick: (blob: any) => () => any;
        setDescriptionRows: (value: any, width: number) => number;
    }

    export interface State {
        images: any[],
        imageComps: any[],
        expanded: boolean,
        descriptionHeight: any;
        openDialog: boolean,
        dialogMessage: string,
        dialogType: string,
        deleteIndex: number
    }
}
class ObservationInspector extends React.Component<ObservationInspector.Props, ObservationInspector.State> {

    private descriptionInput: any;
    private deleteIcon = <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1">
        <title>Delete</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Group-17" transform="translate(-2.000000, -2.000000)">
                <circle id="Oval-2" fill="#FFFFFF" fillRule="nonzero" cx="11.5" cy="12.5" r="7.5"></circle>
                <g id="ic_cancel_24px">
                    <g id="Group">
                        <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M12,2 C6.47,2 2,6.47 2,12 C2,17.53 6.47,22 12,22 C17.53,22 22,17.53 22,12 C22,6.47 17.53,2 12,2 L12,2 Z M17,15.59 L15.59,17 L12,13.41 L8.41,17 L7,15.59 L10.59,12 L7,8.41 L8.41,7 L12,10.59 L15.59,7 L17,8.41 L13.41,12 L17,15.59 L17,15.59 Z" id="Shape" fill="#FF0000"></path>
                    </g>
                </g>
            </g>
        </g>
    </svg>
    private pictureInput: HTMLInputElement | null = null;
    private imgPlaceholder: any = <svg width="100px" height="100px" viewBox="0 0 101 101" version="1.1">
        <title>Image Placeholder</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Group-16" transform="translate(2.000000, 2.000000)" strokeWidth="3">
                <g id="Page-1-Copy-16" stroke="#A6A8AB">
                    <polyline id="Stroke-1" points="97 90 97 97 90 97"></polyline>
                    <path d="M78,97 L13,97" id="Stroke-3" strokeDasharray="10.142,10.142"></path>
                    <polyline id="Stroke-5" points="7 97 0 97 0 90"></polyline>
                    <path d="M0,78 L0,13" id="Stroke-7" strokeDasharray="10.142,10.142"></path>
                    <polyline id="Stroke-9" points="0 7 0 0 7 0"></polyline>
                    <path d="M19,0 L84,0" id="Stroke-11" strokeDasharray="10.142,10.142"></path>
                    <polyline id="Stroke-13" points="90 0 97 0 97 7"></polyline>
                    <path d="M97,19 L97,84" id="Stroke-15" strokeDasharray="10.142,10.142"></path>
                </g>
                <g id="image-copy-20" transform="translate(27.000000, 25.000000)" stroke="#A4A8AB" strokeLinecap="round" strokeLinejoin="round">
                    <rect id="Rectangle-path" x="0" y="0" width="44" height="44" rx="2"></rect>
                    <circle id="Oval" cx="13.5" cy="13.5" r="3.5"></circle>
                    <polyline id="Shape" points="44 29.2727273 31.8125 17 5 44"></polyline>
                </g>
            </g>
        </g>
    </svg>

    state: ObservationInspector.State =
        {
            expanded: false,
            images: [],
            imageComps: [<div key={0}>{this.imgPlaceholder}</div>, <div key={1}>{this.imgPlaceholder}</div>, <div key={2}>{this.imgPlaceholder}</div>],
            descriptionHeight: 0,
            openDialog: false,
            dialogMessage: '',
            dialogType: '',
            deleteIndex: -1
        }

    shouldComponentUpdate(nextProps: any, nextState: any) {
        if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
            return false;
        } else {
            return true;
        }
    }

    componentDidMount() {
        this.getImages();
        if (this.props.observation.copy) {
            this.setState({ expanded: true })
        }
    }
    /**
     * Resizes the the image taken to 500px and reduces quality to 30% of its original quality
     *
     * @param {Model.IResizeImageOptions} settings The settings object
     * @return {Promise} a promise with the blob resized
     */
    private resizeImage = (settings: Model.IResizeImageOptions) => {
        const file = settings.file;
        const maxSize = settings.maxSize;
        const reader = new FileReader();
        const image = new Image();
        const canvas = document.createElement('canvas');
        const dataURItoBlob = (dataURI: string) => {
            const bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
                atob(dataURI.split(',')[1]) :
                unescape(dataURI.split(',')[1]);
            const mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
            const max = bytes.length;
            const ia = new Uint8Array(max);
            for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
            return new Blob([ia], { type: mime });
        };
        const resize = () => {
            let width = image.width;
            let height = image.height;

            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }

            canvas.width = width;
            canvas.height = height;
            const ctx = canvas.getContext('2d')
            if (ctx) {
                ctx.translate(canvas.width / 2, canvas.height / 2)
                const orientation = window.orientation;
                if (orientation === 0) {
                    ctx.rotate(Math.PI / 2)

                }
                else if (orientation === -90) {
                    ctx.rotate(Math.PI)
                }
                else if (orientation === 180) {
                    ctx.rotate(3 * Math.PI / 2)
                }
                ctx.translate(-canvas.width / 2, -canvas.height / 2)
                ctx.drawImage(image, 0, 0, width, height);
            }
            let dataUrl = canvas.toDataURL('image/jpeg', 0.3);
            return dataURItoBlob(dataUrl);

        };

        return new Promise((ok, no) => {
            if (!file.type.match(/image.*/)) {
                no(new Error("Not an image"));
                return;
            }

            reader.onload = (readerEvent: any) => {
                image.onload = () => ok(resize());
                image.src = readerEvent.target.result;
            };
            reader.readAsDataURL(file);
        })
    }

    private handleDelete = () => {
        this.props.onDelete(this.props.id, this.props.catId);
    }

    private handleCopy = () => {
        this.props.onCopy(this.props.id, this.props.question, this.props.observation)
    }

    private handleExpand = (e?: any) => {
        if (e) {
            e.preventDefault();
        }
        const exp = this.state.expanded
        this.getImages();
        this.setState({ expanded: !exp })
    }

    private unloadBlob = (event: React.SyntheticEvent<HTMLImageElement>) => {
        const target = event.target as HTMLImageElement
        URL.revokeObjectURL(target.src)
    }
    private handleDialogOpen = (type: string, index?: number) => (e?: any) => {
        if (e) {
            e.stopPropagation();
        }
        let msg = "";

        switch (type) {
            case 'deleteImg':
                msg = "Are you sure you want to delete the image?"
                break;
            case 'deleteObs':
                msg = "Are you sure you want to delete the observation?"
                break;
            case 'overrideImg':
                msg = "Your third image will be overwritten. Are you sure you want to continue?";
                break;
        }
        let stateObj = {}
        if (index) {
            stateObj = { deleteIndex: index }
        }
        this.setState({ ...stateObj, openDialog: true, dialogType: type, dialogMessage: msg });
    }
    private getImages = async () => {
        let _imageArr: any = [];
        if (this.props.observation.images) {
            this.props.getImages(this.props.observation.images)
                .then((imgs: any) => {
                    for (let i = 0; i < 3; i++) {
                        if (imgs[i]) {
                            const dataView = new DataView(imgs[i].imageURL);
                            const blob = new Blob([dataView], { type: 'image/jpeg' });
                            const image = new Image()
                            image.src = URL.createObjectURL(blob)
                            _imageArr.push(<div onClick={this.props.imageClick(blob)} key={i} style={{ display: 'inline-flex', width: 110, height: 110, position: 'relative' }} >
                                <img onLoad={this.unloadBlob} src={image.src} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
                                {this.state.expanded && <IconButton style={{ color: 'red', position: 'absolute', right: '0' }} onClick={this.handleDialogOpen('deleteImg', i)} >
                                    <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1">
                                        <title>Delete</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                            <g id="Group-17" transform="translate(-2.000000, -2.000000)">
                                                <circle id="Oval-2" fill="#FFFFFF" fillRule="nonzero" cx="11.5" cy="12.5" r="7.5"></circle>
                                                <g id="ic_cancel_24px">
                                                    <g id="Group">
                                                        <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12,2 C6.47,2 2,6.47 2,12 C2,17.53 6.47,22 12,22 C17.53,22 22,17.53 22,12 C22,6.47 17.53,2 12,2 L12,2 Z M17,15.59 L15.59,17 L12,13.41 L8.41,17 L7,15.59 L10.59,12 L7,8.41 L8.41,7 L12,10.59 L15.59,7 L17,8.41 L13.41,12 L17,15.59 L17,15.59 Z" id="Shape" fill="#FF0000"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </IconButton>}
                            </div>);
                        } else {
                            _imageArr.push(<div key={i}>{this.imgPlaceholder}</div>)
                        }
                    }
                    this.setState({ images: imgs, imageComps: _imageArr });
                })
        }

    }
    private handleDescriptionSelect = (value: any) => {
        this.descriptionInput.value = value.name;
        const _rows = this.props.setDescriptionRows(value.name, this.descriptionInput.offsetWidth);
        this.setState({ descriptionHeight: _rows })
        this.props.handleDescriptionSelect(value.name, this.props.id, this.props.catId, this.props.question);
    }

    private handleDialogClose = () => {
        this.setState({ openDialog: false });
    }
    private handleDialogCloseClick = () => {
        switch (this.state.dialogType) {
            case 'deleteImg':
                this.deleteImage()
                break;
            case 'deleteObs':
                this.handleDelete()
                break;
            case 'overrideImg':
                this.confirmTakePicture();
                break;

        }
        this.setState({ openDialog: false })
    }

    private confirmTakePicture = () => {
        if (this.pictureInput) {
            this.pictureInput.click();
        }
    }

    private handleDescriptionChange = (e: any) => {
        if (this.state.descriptionHeight > 0) {
            this.setState({ descriptionHeight: 0 })
        }
        this.props.handleDescriptionChange(e, this.props.id, this.props.catId, this.props.question)
    }
    private cameraClick = () => {
        if (this.pictureInput) {
            if (this.props.observation.images && this.state.images.length >= 3) {
                this.handleDialogOpen('overrideImg')()
            } else {
                this.pictureInput.click();

            }
        }
    }

    private takePicture = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const input = e.target;
        const reader = new FileReader();
        let _images = [] as any[];
        if (this.props.observation.images) {
            _images = await localforage.getItem(this.props.observation.images) as any[];
        }
        if (input.files && input.files[0]) {
            this.resizeImage({ file: input.files[0], maxSize: 500 })
                .then((res: any) => {
                    reader.onload = (e: any) => {
                        if (this.pictureInput && e.target) {
                            let _imageArr: any = _images.slice();
                            if (_imageArr.length === 3 && input.files && input.files[0]) {
                                _imageArr[2] = { imageURL: e.target.result };
                            } else if (input.files && input.files[0]) {
                                _imageArr[_imageArr.length] = { imageURL: e.target.result };
                            }
                            let _imageComps = [];
                            for (let i = 0; i < 3; i++) {
                                if (_imageArr[i]) {
                                    const dataView = new DataView(_imageArr[i].imageURL);
                                    const blob = new Blob([dataView], { type: 'image/jpeg' });
                                    const image = new Image();
                                    image.src = URL.createObjectURL(blob)
                                    _imageComps.push(<div onClick={this.props.imageClick(blob)} key={i} style={{ display: 'inline-flex', width: 110, height: 110, position: 'relative' }} >
                                        <img onLoad={this.unloadBlob} src={image.src} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
                                        <IconButton disableRipple style={{ color: 'red', position: 'absolute', right: '0' }} onClick={this.handleDialogOpen('deleteImg', i)}>
                                            {this.deleteIcon}
                                        </IconButton>
                                    </div>);
                                }
                                else {
                                    _imageComps.push(<div key={i}>{this.imgPlaceholder}</div>)
                                }
                            }
                            this.setState({ images: _imageArr, imageComps: _imageComps });
                            this.props.handleImagesChange(_imageArr, this.props.id, this.props.catId, this.props.question, this.props.observation);
                            this.pictureInput.value = '';
                        }
                    }
                    reader.readAsArrayBuffer(res);
                })
        }
    }

    private deleteImage = () => {
        //e.stopPropagation();
        // if (confirm("Are you sure you want to delete this picture?")) {
        this.props.handleImageDelete(this.state.deleteIndex, this.props.id, this.props.catId, this.props.question, this.props.observation)
            .then(() => {
                this.getImages();
            })
        // }
    }

    private handleContinue = () => {
        this.props.handleObservationContinue(this.props.id, this.props.catId, this.props.question, this.props.observation);
        this.handleExpand();
    }

    private setRiskStyle = (option: any): React.CSSProperties | undefined => {
        let _style = { margin: '6px 0px', flexGrow: 1, boxShadow: 'none', border: '#ccc solid 1px', borderRadius: 0 } as React.CSSProperties;
        if (this.props.observation.risk.value && this.props.observation.risk.value === option.value) {

            if (option.value === "1 Low") {
                _style.backgroundColor = '#ffff80'
            } else if (option.value === "2 Medium") {
                _style.backgroundColor = '#ffa64d'
            } else if (option.value === "3 High") {
                _style.backgroundColor = '#ff704d'
            }
            _style.fontWeight = 'bold'

        } else {
            _style.backgroundColor = 'white'
        }
        return _style
    }

    render() {
        return <div style={{ marginBottom: 20, padding: 15, overflow: 'hidden', border: '1px solid grey ', backgroundColor: 'white', WebkitOverflowScrolling: 'touch' }}>
            <div style={{ display: 'flex' }}>
                <div style={{ marginRight: 10, verticalAlign: 'middle', width: '50%' }} id="questionBox">
                    <div>
                        {this.props.question.cat}
                    </div>
                    <div>
                        {this.props.question.text}
                    </div>
                </div>

                <div style={{ width: '100%', display: 'inline-flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
                    {this.state.imageComps}
                </div>

                {this.state.expanded &&
                    <React.Fragment>
                        <div onClick={this.cameraClick} style={{ alignContent: 'center', alignSelf: 'center', boxSizing: 'border-box', display: 'inline-flex', marginLeft: 10, borderRadius: 20, boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)' }}>
                            <svg style={{ borderRadius: 20 }} width="100px" height="100px" viewBox="0 0 157 157" version="1.1">
                                <title>Group 9</title>
                                <desc>Created with Sketch.</desc>
                                <defs></defs>
                                <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="Group-9">
                                        <g id="Group-2-Copy-9" fill="#FFFFFF">
                                            <rect id="Rectangle" x="0" y="0" width="157" height="157" rx="15"></rect>
                                        </g>
                                        <g id="camera-copy-8" transform="translate(38.000000, 45.000000)" stroke="#314474" strokeLinecap="round" strokeLinejoin="round" strokeWidth="6">
                                            <path d="M82,59.5555556 C82,63.6670087 78.6624863,67 74.5454545,67 L7.45454545,67 C3.33751368,67 8.276208e-16,63.6670087 0,59.5555556 L0,18.6111111 C-4.138104e-16,14.499658 3.33751368,11.1666667 7.45454545,11.1666667 L22.3636364,11.1666667 L29.8181818,0 L52.1818182,0 L59.6363636,11.1666667 L74.5454545,11.1666667 C78.6624863,11.1666667 82,14.499658 82,18.6111111 L82,59.5555556 Z" id="Shape"></path>
                                            <circle id="Oval" cx="41" cy="37" r="15"></circle>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <input style={{ visibility: 'hidden', position: 'absolute' }} onChange={this.takePicture} ref={e => this.pictureInput = e} type="file" name="myImage" accept="image/x-png,image/gif,image/jpeg,image/png,image/jpg" />
                    </React.Fragment>
                }

                <div style={this.props.observation.copy ? { display: 'none' } : { display: 'inline-flex', verticalAlign: 'middle', justifyContent: 'center', width: '10%' }}>
                    <IconButton disableRipple onClick={this.handleExpand} style={this.state.expanded ? {} : { display: 'none' }}><UpIcon></UpIcon></IconButton>
                    <IconButton disableRipple onClick={this.handleExpand} style={this.state.expanded ? { display: 'none' } : {}}><DownIcon></DownIcon></IconButton>
                </div>



            </div>
            <div style={this.state.expanded ? {} : { display: 'none' }}>


                <FormControlLabel style={{ marginLeft: '-3px' }}
                    control={<Checkbox style={{ width: '25px', color: 'rgb(225,0,80)' }} onChange={this.props.handleFlaggedChange(this.props.id, this.props.catId, this.props.question)} checked={this.props.observation.flagged}>Flagged</Checkbox>}
                    label="Flagged for review">
                </FormControlLabel>
                <InputLabel style={{ display: 'block', fontSize: '12px' }}>Risk Level</InputLabel>
                <div style={{ marginTop: '10px', display: 'flex' }}>{this.props.riskLevels.map((option, optionIndex) => {
                    return <ButtonMaterial disableRipple onClick={this.props.handleRiskChange(this.props.id, this.props.catId, this.props.question)} style={this.setRiskStyle(option)} key={optionIndex} variant="raised" >{option.value}</ButtonMaterial>
                })}
                </div>
                <FormControl style={{ marginBottom: 20, marginTop: 10 }} fullWidth>
                    <InputLabel shrink htmlFor="labInput">Select Lab</InputLabel>
                    <Select
                        style={{ marginTop: 16 }}
                        searchable
                        clearable={false}
                        id="labInput"
                        placeholder="Select a lab"
                        closeOnSelect={true}
                        options={this.props.labs.map((object, i) => ({
                            name: object.name, value: i.toString(), _id: object._id
                        }))}
                        onChange={this.props.handleLabChange(this.props.id, this.props.catId, this.props.question)}
                        value={this.props.labs.findIndex(lab => lab.name === this.props.observation.space.value)}
                        labelKey="name" />
                </FormControl>
                <FormControl style={{ marginBottom: 20 }} fullWidth>
                    <InputLabel shrink={true} htmlFor="multiOrgSelect" >Pick Audited Department(s) </InputLabel>
                    <Select
                        style={{ marginTop: 16 }}
                        searchable
                        id="multiOrgSelect"
                        placeholder="Select One or more Department"
                        closeOnSelect={false}
                        multi={true}
                        options={this.props.organizations.map((object, i) => ({
                            name: object.name, value: i.toString(), _id: object._id, space: object.space
                        }))}
                        onChange={this.props.handleAuditedDepartmentsChange(this.props.id, this.props.catId, this.props.question)}
                        value={this.props.observation.auditedOrgs}
                        labelKey="name" />
                </FormControl>
                <FormControl fullWidth style={{ marginBottom: 20 }}>
                    <InputLabel shrink={true} htmlFor="benchInput" >Bench/Location </InputLabel>
                    <Input
                        disableUnderline
                        multiline
                        inputProps={{ id: 'benchInput', maxLength: 1000 }}
                        defaultValue={this.props.observation ? this.props.observation.bench : ""}
                        onChange={this.props.handleBenchChange(this.props.id, this.props.catId, this.props.question)}
                        style={{ textAlign: 'center', border: 'lightgrey 1px solid', borderRadius: 4, paddingLeft: 5, paddingRight: 5 }}></Input>
                </FormControl>
                <FormControl fullWidth style={{ marginBottom: 20 }}>
                    <InputLabel shrink={true} htmlFor="descriptionInput" >Observation/Action</InputLabel>
                    <div className="textareaDiv" style={{ marginTop: 15 }}>
                        <Select
                            style={{ position: 'absolute' }}
                            id="descriptionInput"
                            closeOnSelect={true}
                            searchable={false}
                            clearable={false}
                            options={this.props.question.preCannedResponses}
                            labelKey="name"
                            onChange={this.handleDescriptionSelect}
                            value="" />
                        <Input
                            disableUnderline
                            multiline
                            // rows={this.state.descriptionRows}
                            inputProps={{ id: 'descriptionInput', maxLength: 1000, style: this.state.descriptionHeight > 0 ? { height: this.state.descriptionHeight } : {} }}
                            defaultValue={this.props.observation.description}
                            onChange={this.handleDescriptionChange}
                            placeholder="Type an observation or select a pre-defined one (when applicable)"
                            inputRef={(e) => { this.descriptionInput = e }}
                            style={{ textAlign: 'center', borderRadius: '4px 0px 0px 4px', paddingLeft: 5, position: 'relative', paddingRight: 5, width: 'calc(100% - 28px)', top: 0, minHeight: this.state.descriptionHeight > 0 ? this.state.descriptionHeight + 15 : 36, boxSizing: 'border-box', border: '1px lightgrey solid', backgroundColor: 'white' }}></Input>
                    </div>
                </FormControl>
                <FormControl style={{ marginBottom: 20 }} fullWidth>
                    <InputLabel shrink={true} htmlFor="userActionInput">Corrective Action</InputLabel>
                    <Select
                        clearable={false}
                        searchable={false}
                        style={{ marginTop: 16 }}
                        id="userActionInput"
                        placeholder="Choose Corrective Action"
                        closeOnSelect={true}
                        options={this.props.correctionActions.map((ca, i) => ({
                            label: ca.value as string,
                            value: i
                        }))}
                        onChange={this.props.handleCorrectiveActionChange(this.props.id, this.props.catId, this.props.question)}
                        value={this.props.correctionActions.findIndex(ca => ca.value === this.props.observation.action.value)}
                    />
                </FormControl>
                {this.props.observation.action.value && this.props.observation.action.value === 'User Action Required' &&
                    <FormControl style={{ marginBottom: 20 }} fullWidth>
                        <InputLabel shrink={true} htmlFor="rpSelect" >Responsible Person</InputLabel>
                        <Select
                            style={{ marginTop: 16 }}
                            searchable
                            id="rpSelect"
                            placeholder="Type to search for a LSC"
                            closeOnSelect={true}
                            options={this.props.responsiblePersons.map(({ name }, i) => ({
                                label: name,
                                value: i
                            }))}
                            onChange={this.props.handleResponsiblePersonChange(this.props.id, this.props.catId, this.props.question)}
                            //@ts-ignore
                            value={this.props.responsiblePersons.findIndex(rp => rp.name === this.props.observation.responsible.value)}
                        />
                    </FormControl>
                }

                {this.props.observation.action.value && this.props.observation.action.value === 'Corrected On-site' &&
                    <FormControl style={{ marginBottom: 20 }} fullWidth>
                        <InputLabel shrink={true} htmlFor="organizationInput">Corrected-by Organization</InputLabel>
                        <Select
                            style={{ marginTop: 16 }}
                            searchable
                            id="organizationInput"
                            placeholder="Type to search for an Organization"
                            closeOnSelect={true}
                            options={this.props.organizations.map(({ name }, i) => ({
                                label: name,
                                value: i
                            }))}

                            onChange={this.props.handleOrganizationChange(this.props.id, this.props.catId, this.props.question)}
                            //@ts-ignore
                            value={this.props.organizations.findIndex(org => org.name === this.props.observation.org.value)}
                        />
                    </FormControl>
                }
                {this.props.observation.action.value && this.props.observation.action.value === 'Create Work Order' &&
                    <FormControl style={{ marginBottom: 20 }} fullWidth>
                        <InputLabel shrink={true} htmlFor="rcSelect">Request Class</InputLabel>

                        <Select
                            style={{ marginTop: 16 }}
                            searchable
                            id="rcSelect"
                            placeholder="Type to search for request class"
                            closeOnSelect={true}
                            options={this.props.requestClasses.map(({ parent, name }, i) => ({
                                label: parent ? `${parent}/${name}` : name,
                                value: i
                            }))}
                            onChange={this.props.handleRequestClassChange(this.props.id, this.props.catId, this.props.question)}
                            //@ts-ignore
                            value={this.props.requestClasses.findIndex(rc => rc.name === this.props.observation.reqclass.value)}
                            menuStyle={{ height: 100 }}
                        />
                    </FormControl>
                }

                <ButtonMaterial disableRipple style={{ float: 'right', color: 'white', backgroundColor: 'rgb(191,40,40)' }} variant="raised" onClick={this.handleDialogOpen("deleteObs")}>{this.props.observation.copy ? 'Cancel' : 'Delete'}</ButtonMaterial>
                {this.props.observation.copy ?
                    <ButtonMaterial disableRipple style={{ float: 'right', color: 'white', marginRight: 10 }} color="secondary" variant="raised" onClick={this.handleContinue}>Continue</ButtonMaterial>
                    :
                    <ButtonMaterial disableRipple style={{ float: 'right', color: 'white', marginRight: 10 }} color="secondary" variant="raised" onClick={this.handleCopy}>Copy</ButtonMaterial>
                }
            </div>
            <div style={this.props.observation ? { width: '48px', margin: '0 auto' } : { display: 'none' }}>
                <IconButton disableRipple onClick={this.handleExpand} style={this.props.observation && this.state.expanded && !this.props.observation.copy ? {} : { display: 'none' }}><UpIcon onClick={this.handleExpand}></UpIcon></IconButton>
            </div>
            <Dialog open={this.state.openDialog} onClose={this.handleDialogClose}>
                <DialogTitle>
                    {this.state.dialogMessage}
                </DialogTitle>
                <DialogActions>
                    <ButtonMaterial variant="outlined" onClick={this.handleDialogClose} disableRipple>Cancel</ButtonMaterial>
                    <ButtonMaterial variant="raised" onClick={this.handleDialogCloseClick} disableRipple>Confirm</ButtonMaterial>
                </DialogActions>
            </Dialog>
        </div>;
    }
}
export default ObservationInspector;