import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import App from './App';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import { Breakpoint } from '@material-ui/core/styles/createBreakpoints';
import createMuiTheme, { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { Overrides } from '@material-ui/core/styles/overrides';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    overrides?: Overrides;

  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    overrides?: Overrides
  }
}
function createMyTheme(options: ThemeOptions) {
  return createMuiTheme({

    ...options,
  })
}

class LabAudit extends HTMLElement {
  constructor() {
    const theme = createMyTheme({
      overrides: {
        MuiTableRow: { selected: { backgroundColor: 'rgba(9,159,214,0.3) !important' } },
        MuiTab: { root: { maxWidth: 'none' } },
        MuiTabs: { root: { backgroundColor: 'white' } }
      },
        palette:{primary:{main:'rgb(21,69,118)'}, secondary:{main:'rgb(99,177,219)'}}
    })
    super();
    ReactDOM.render(
      <HashRouter>
        <MuiThemeProvider theme={theme}>
          <App />
        </MuiThemeProvider>
      </HashRouter>, this);
  }
}

customElements.define('lab-audit-react', LabAudit);