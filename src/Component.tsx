import * as React from 'react';
import * as _ReactModal from 'react-modal';


const ReactModal = (_ReactModal as any).default as typeof _ReactModal;


export module Modal {
  export interface Props {
    isOpen: boolean;
    zIndex: number
  }
}
export class Modal extends React.PureComponent<Modal.Props> {
  static setAppElement(selector: string) {
    ReactModal.setAppElement(selector);
  }

  render() {

    return <ReactModal className="" style={{
      content: {
        left: '0',
        top: '0',
        bottom: 'auto',
        width: '100%',
        height: '100%',
        overflow: 'scroll',
        position: 'absolute',
        boxSizing: 'border-box',
        padding: "0 20px",
        zIndex: this.props.zIndex
      },
      overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        // backgroundColor: 'rebeccapurple',
        zIndex: this.props.zIndex

      }
    }} isOpen={this.props.isOpen}
      shouldCloseOnOverlayClick={false}
      shouldCloseOnEsc={false}>
      {this.props.children}
    </ReactModal>;
  }
}

const uuid = (function () {
  let seed = 1;
  return function () {
    return 'uuid-' + seed++;
  };
})();

export const TDR: React.StatelessComponent<{ style?: React.CSSProperties }> = props =>
  <td style={{
    ...props.style,
    textAlign: 'right',
    width: '140px',
    paddingRight: '2px'
  }}>{props.children}</td>;

export module ToggleSection {
  export interface Props {
    toggleButtonStyle?: React.CSSProperties;
  }
  export interface State {
    open: boolean;
  }
}

export class ToggleSection extends React.Component<ToggleSection.Props, ToggleSection.State> {
  private static DownArrow() {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M1 0 L0 0 L0.5 1 Z" stroke="none" />
    </svg>;
  }

  private static readonly RightArrow = function () {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M0 0 L0 1 L1 0.5 Z" stroke="none" />
    </svg>;
  }()

  private static UpArrow() {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M0 1 L1 1 L0.5 0 Z" stroke="none" />
    </svg>;
  }

  private static readonly Button = (function () {
    const style: React.CSSProperties = {
      background: 'none',
      border: '0',
      padding: '5px',
      fontSize: '24px',
      display: 'block',
      width: '100%',
      textAlign: 'left'
    };
    return function Button(props: React.HTMLProps<HTMLButtonElement>) {
      return <button {...props}
        style={{ ...props.style, ...style }} onClick={props.onClick}>{props.children}</button>;
    };
  })();

  state: ToggleSection.State = { open: false };

  private handleToggle = () => {
    this.setState({ open: !this.state.open });
  }

  render() {
    const children = React.Children.toArray(this.props.children);
    if (children.length === 0)
      return null;

    return <React.Fragment>
      <ToggleSection.Button style={{ ...this.props.toggleButtonStyle }} onClick={this.handleToggle}>
        {this.state.open ? ToggleSection.DownArrow() : ToggleSection.RightArrow}
        &nbsp;{children[0]}
      </ToggleSection.Button>
      <div style={{ display: this.state.open ? 'block' : 'none' }}>
        {children.slice(1)}
        <ToggleSection.Button style={{ ...this.props.toggleButtonStyle }} onClick={this.handleToggle}>
          {this.state.open ? ToggleSection.UpArrow() : ToggleSection.RightArrow}
          &nbsp;{children[0]}
        </ToggleSection.Button>
        <hr />
      </div>
    </React.Fragment>;
  }
}




export class ToggleSectionObservation extends React.Component<ToggleSection.Props, ToggleSection.State> {
  private static DownArrow() {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M1 0 L0 0 L0.5 1 Z" stroke="none" />
    </svg>;
  }

  private static readonly RightArrow = function () {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M0 0 L0 1 L1 0.5 Z" stroke="none" />
    </svg>;
  }()

  private static UpArrow() {
    return <svg style={{ width: '16px', height: '16px' }} viewBox="0 0 1 1">
      <path d="M0 1 L1 1 L0.5 0 Z" stroke="none" />
    </svg>;
  }

  private static readonly Button = (function () {
    const style: React.CSSProperties = {
      background: 'none',
      border: '0',
      padding: '5px',
      fontSize: '24px',
      display: 'block',
      width: '100%',
      textAlign: 'left'
    };
    return function Button(props: React.HTMLProps<HTMLButtonElement>) {
      return <button {...props}
        style={{ ...props.style, ...style }} onClick={props.onClick}>{props.children}</button>;
    };
  })();

  state: ToggleSection.State = { open: false };

  private handleToggle = () => {
    this.setState({ open: !this.state.open });
  }

  render() {
    const children = React.Children.toArray(this.props.children);
    if (children.length === 0)
      return null;

    return <React.Fragment>
      <ToggleSectionObservation.Button style={{ ...this.props.toggleButtonStyle }} onClick={this.handleToggle}>
        {this.state.open ? ToggleSectionObservation.DownArrow() : ToggleSectionObservation.RightArrow}
        &nbsp;{children[0]}
      </ToggleSectionObservation.Button>
      <div style={{ display: this.state.open ? 'block' : 'none' }}>
        {children.slice(1)}
        <ToggleSectionObservation.Button style={{ ...this.props.toggleButtonStyle }} onClick={this.handleToggle}>
          {this.state.open ? ToggleSectionObservation.UpArrow() : ToggleSectionObservation.RightArrow}
          &nbsp;{children[0]}
        </ToggleSectionObservation.Button>
        <hr />
      </div>
    </React.Fragment>;
  }
}




export module RadioGroup {
  export interface Props {
    options: {
      label: string;
      value: string;
    }[];
    selected?: number | null;
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  }
}

export class RadioGroup extends React.PureComponent<RadioGroup.Props> {
  render() {
    const groupId = uuid();
    return <React.Fragment>
      {this.props.options.map((o, i) => {
        const optionId = uuid();
        return <div key={i} style={{
          display: 'inline-block',
          position: 'relative',
          marginRight: '10px'
        }}>
          <input id={optionId} checked={i === this.props.selected}
            type="radio" value={o.value} name={groupId}
            onChange={this.props.onChange} />
          <label htmlFor={optionId}>{o.label}</label>
        </div>;
      })}
    </React.Fragment>;
  }
}

export module TabView {
  export interface Props { }
  export interface State {
    selectedTab: number;
  }
}

export class TabView extends React.Component<TabView.Props, TabView.State> {
  static Tab: React.StatelessComponent<{ label: string }> = props => <React.Fragment />;

  state: TabView.State = {
    selectedTab: 0
  };

  render() {
    const children = React.Children.toArray(this.props.children);

    const buttons: JSX.Element[] = [];
    const contents: JSX.Element[] = [];

    for (let i = 0; i < children.length; ++i) {
      const child = children[i];

      if (typeof child === 'object' && child.type === TabView.Tab) {
        const label = child.props.label;
        if (label) {
          buttons.push(<button key={i} style={{
            flexGrow: 1,
            color: i === this.state.selectedTab ? 'white' : 'black',
            background: i === this.state.selectedTab ? 'rgb(29, 54, 73)' : 'none',
            border: '1px solid rgb(29, 54, 73)',
            margin: '0',
            padding: '0',
            height: '44px'
          }} value={i} onClick={this.handleTabClick}>{label}</button>);
          contents.push(<div key={i} style={{
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            height: '100%',
            paddingTop: '20px',
            display: i === this.state.selectedTab ? 'block' : 'none'
          }}>{child.props.children}</div>);
        }
      }
    }

    return <div>
      <div style={{ display: 'flex' }}>
        {buttons}
      </div>
      <div style={{ position: 'relative' }}>
        {contents}
      </div>
    </div>;
  }

  private handleTabClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    const target = event.target as HTMLButtonElement;
    this.setState({ selectedTab: Number(target.value) });
  }
}

export module TabledInput {
  export interface Props {
    label: string;
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  }
}

export class TabledInput extends React.PureComponent<TabledInput.Props> {
  render() {
    const inputId = uuid();
    return <React.Fragment>
      <TDR><label htmlFor={inputId}>{this.props.label}</label></TDR>
      <td style={{ position: 'relative' }}>
        <input style={{ width: '95%' }} id={inputId} type="input"
          onChange={this.props.onChange} />
      </td>
    </React.Fragment>;
  }
}

export module LabeledInput {
  export interface Props {
    value?: string;
    label: string;
    onChange?: React.ChangeEventHandler<HTMLTextAreaElement>
  }
}

export class LabeledInput extends React.PureComponent<LabeledInput.Props> {
  render() {
    const inputId = uuid();
    return <React.Fragment>
      <div><label htmlFor={inputId} style={{ paddingRight: 10 }}>{this.props.label}</label></div>
      <div>
        <textarea value={this.props.value} style={{
          width: '100%',
          height: '44px'
        }} id={inputId} onChange={this.props.onChange} />
      </div>
    </React.Fragment>;
  }
}

export module LabeledSelect {
  export interface Props {
    label: string;
    options: {
      value: string;
      label: string;
    }[];
    selected?: number;
    onChange: React.ChangeEventHandler<HTMLSelectElement>;
  }
}

export class LabeledSelect extends React.PureComponent<LabeledSelect.Props> {
  readonly id = uuid();

  render() {
    const props = this.props;
    return <React.Fragment>
      <TDR><label htmlFor={this.id}>{props.label}</label></TDR>
      <td>
        <select id={this.id} onChange={props.onChange} value={(props.selected || 0).toString()}>
          {props.options.map((option, i) => (

            <option key={i} label={option.label} value={option.value}>{option.label}</option>
          ))}
        </select></td>
    </React.Fragment>;
  }
}


export module LabeledCheckBox {
  export interface Props {
    label?: string;
    value?: string;
    checked?: boolean;
    onChange?: React.ChangeEventHandler<HTMLInputElement>;

    labelStyle?: React.CSSProperties;
  }
}

export class LabeledCheckBox extends React.PureComponent<LabeledCheckBox.Props> {
  private readonly id = uuid();

  static defaultProps: Partial<LabeledCheckBox.Props> = {
    checked: false,
    value: '',
    label: '',
  };

  render() {
    const props = this.props;

    return <React.Fragment>
      <TDR><label style={{ ...props.labelStyle }} htmlFor={this.id}>{props.label}</label></TDR>
      <td><input id={this.id} type="checkbox" checked={props.checked}
        onChange={this.props.onChange} value={props.value} /></td>
    </React.Fragment>;
  }
}

export module Toolbar {
  export interface Props {
    style?: React.CSSProperties;
  }
}

export class Toolbar extends React.PureComponent<Toolbar.Props> {
  static readonly Spacer = (props: { flexGrow?: number; }) =>
    <div style={{ flexGrow: props.flexGrow || 1 }} />

  static readonly Text: React.StatelessComponent<{ style?: React.CSSProperties; }> = props => {
    return <div style={{ display: 'flex', padding: '0 5px' }}>
      <div style={{ margin: 'auto' }}>
        {props.children}
      </div>
    </div>;
  }

  static readonly Button: React.StatelessComponent<{ style?: React.CSSProperties; }> = props => {
    return <div style={{ display: 'flex' }}>
      <div style={{ margin: 'auto' }}>
        {props.children}
      </div>
    </div>;
  }

  render() {
    return <div style={{
      color: 'white',
      minHeight: '64px',
      backgroundColor: 'rgb(21,69,118)',
      borderBottom: '1px solid black',
      padding: '0 10px',
      top: '0px',
      display: 'flex',
      ...this.props.style
    }}>
      {this.props.children}
    </div>;
  }
}

export const HomeIcon: React.StatelessComponent<React.SVGProps<SVGElement>> = (props) => {
  return <svg width="28px" height="33px" viewBox="0 0 28 33" version="1.1" >
    <title>Home</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-8" transform="translate(1.000000, 2.000000)" stroke="#FFFFFF" strokeWidth="2">
        <polygon id="Path-8" points="12.528995 0 0 9.89698876 0 29.0928594 15.7910296 29.0928594 25.1510663 29.0928594 25.1510663 9.89698876"></polygon>
        <polygon id="Path-10" points="10 29.1570835 10 20 15.8537191 20 15.8537191 29.1570835"></polygon>
      </g>
    </g>
  </svg>;
};

export const SignOutIcon: React.StatelessComponent<React.SVGProps<SVGElement>> = (props) => {
  return <svg width="26px" height="31px" viewBox="0 0 26 31" version="1.1">
    <title>Sign Out</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-7" transform="translate(1.000000, 1.000000)" stroke="#FFFFFF" strokeWidth="2">
        <polyline id="Path-4" points="18.226791 6.80620646 18.226791 0 0 0 0 28.8173953 18.226791 28.8173953 18.226791 23.177691"></polyline>
        <path d="M8,15 L24,15" id="Shape" strokeLinecap="round" strokeLinejoin="round"></path>
        <polyline id="Shape" strokeLinecap="round" strokeLinejoin="round" points="18 9 24 15 18 21"></polyline>
      </g>
    </g>
  </svg>;
};

export const Button: React.StatelessComponent<React.HTMLProps<HTMLButtonElement>> = (props) => {
  return <button {...props}
    style={{ cursor: 'pointer', background: 'none', border: '0', padding: '0', ...props.style }}>
    {props.children}
  </button>;
};

export module Loader {
  export interface Props {
    load: () => Promise<any>;
    label?: string;
  }
  export interface State {
    loaded: boolean;
    slow: boolean;
  }
}

export class Loader extends React.Component<Loader.Props, Loader.State> {
  state: Loader.State = {
    loaded: false,
    slow: false
  };

  async componentDidMount() {
    if (!this.state.loaded) {
      const promise = this.props.load();
      window.setTimeout(() => {
        this.setState({ slow: true });
      }, 2500);
      await promise;
      this.setState({ loaded: true });
    }
  }

  render() {
    if (this.state.loaded)
      return this.props.children;

    if (!this.state.slow)
      return null;

    return <div style={{
      position: 'absolute',
      display: 'flex',
      width: '100%',
      height: 'calc(100% - 65px)'
    }}>
      <div style={{ margin: 'auto' }}>{this.props.label || 'Loading...'}</div>
    </div>;
  }
}

export module ViewFinder {
  export interface Props {
    label: string;
    width: number;
    height: number;
    format?: 'image/png' | 'image/jpeg';
    onImage?: (dataURL: string) => void;
  }
  export interface State {
    stream: MediaStream | null;
  }
}

export class ViewFinder extends React.Component<ViewFinder.Props, ViewFinder.State> {
  private video: HTMLVideoElement | null = null;
  private canvas: HTMLCanvasElement | null = null;
  private button: HTMLButtonElement | null = null;

  static defaultProps: Partial<ViewFinder.Props> = {
    format: 'image/jpeg'
  };

  state: ViewFinder.State = {
    stream: null
  };

  async componentDidMount() {
    const stream = await (async () => {
      try {
        return await navigator.mediaDevices.getUserMedia({
          video: { facingMode: { exact: 'user' } }
        });
      } catch (e) {
        try {
          return await navigator.mediaDevices.getUserMedia({ video: true });
        } catch (e) {
          return null;
        }
      }
    })();

    const video = this.video;
    if (stream && video) {
      video.srcObject = stream;
      video.play();
    }
    this.setState({ stream });
  }

  render() {
    return <div>
      <video ref={e => this.video = e} style={{ display: this.state.stream ? 'block' : 'none' }}
        width={this.props.width} height={this.props.height} />
      <div style={{
        display: !this.state.stream ? 'block' : 'none',
        width: this.props.width,
        height: this.props.height
      }}>No video</div>
      <canvas ref={e => this.canvas = e} style={{ display: 'none' }}
        width={this.props.width} height={this.props.height} />
      <button ref={e => this.button = e} onClick={this.takePicture}>{this.props.label}</button>
    </div>;
  }

  private takePicture = () => {
    if (!this.canvas || !this.video || !this.props.onImage)
      return;

    const ctx = this.canvas.getContext('2d');
    if (!ctx)
      return;

    ctx.drawImage(this.video, 0, 0, this.props.width, this.props.height);

    this.props.onImage(this.canvas.toDataURL(this.props.format));
  }
}
