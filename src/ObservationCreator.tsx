import * as React from 'react';
import * as Model from './Model';
import Select from 'react-select';
import '../src/style.css';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import ButtonMaterial from '@material-ui/core/Button';
import 'react-widgets/dist/css/react-widgets.css';
import { isEqual } from 'lodash';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';




module ObservationCreator {
  export interface Props {

    riskLevels: Model.RiskLevel[];
    correctionActions: Model.WorkOrderAction[];
    responsiblePersons: Model.ResponsiblePerson[];
    labs: Model.Space[];
    requestClasses: Model.RequestClass[];
    organizations: Model.Organization[];
    question: Model.Question
    onCancel?: () => void;
    onCreate: (question: Model.Question, state: Model.NewWorkTask, images: any) => void;
    images?: Model.NewImage[];
    auditedDepartments?: Model.Organization[];
    urls: any;
    imageClick: (e: any) => void
  }

  export interface State {
    flagged: boolean;
    risk: number;
    correctiveAction: number;
    responsiblePerson: number;
    bench: string;
    description: string;
    lab: number;
    organization: number;
    requestClass: number;
    auditedDepartments: Model.Organization[];
    images?: Model.NewImage[],
    preCannedResponses: Model.PreCannedResponse[]
    expanded: boolean,
    descriptionHeight: number,
    openDialog: boolean,
    dialogMessage: string,
    dialogType: string,
    deleteIndex: number
  }
}
class ObservationCreator extends React.Component<ObservationCreator.Props, ObservationCreator.State> {


  private pictureInput: HTMLInputElement | null = null;
  private image: HTMLImageElement | null = null;
  private benchInput: any
  private descriptionInput: any
  private textCanvas: any = document.createElement("canvas");
  // private riskInput: any
  // private auditedOrgsInput: any
  state: ObservationCreator.State =
    {
      flagged: false,
      risk: -1,
      correctiveAction: -1,
      responsiblePerson: -1,
      bench: '',
      description: this.props.question.preCannedResponses && this.props.question.preCannedResponses.length === 1 ? this.props.question.preCannedResponses[0].name : '',
      lab: -1,
      requestClass: -1,
      organization: -1,
      auditedDepartments: [],
      images: this.props.images,
      preCannedResponses: this.props.question.preCannedResponses ? this.props.question.preCannedResponses : [],
      expanded: false,
      descriptionHeight: 0,
      openDialog: false,
      dialogMessage: '',
      dialogType: '',
      deleteIndex: -1
    };
  shouldComponentUpdate(nextProps: any, nextState: any) {
    if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
      return false;
    } else {
      return true;
    }
  }

  private resizeImage = (settings: Model.IResizeImageOptions) => {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement('canvas');
    const dataURItoBlob = (dataURI: string) => {
      const bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
        atob(dataURI.split(',')[1]) :
        unescape(dataURI.split(',')[1]);
      const mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
      const max = bytes.length;
      const ia = new Uint8Array(max);
      for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
      return new Blob([ia], { type: mime });
    };
    const resize = () => {
      let width = image.width;
      let height = image.height;

      if (width > height) {
        if (width > maxSize) {
          height *= maxSize / width;
          width = maxSize;
        }
      } else {
        if (height > maxSize) {
          width *= maxSize / height;
          height = maxSize;
        }
      }

      canvas.width = width;
      canvas.height = height;
      const ctx = canvas.getContext('2d')
      if (ctx) {
        ctx.translate(canvas.width / 2, canvas.height / 2)
        const orientation = window.orientation;
        if (orientation === 0) {
          ctx.rotate(Math.PI / 2)

        }
        else if (orientation === -90) {
          ctx.rotate(Math.PI)
        }
        else if (orientation === 180) {
          ctx.rotate(3 * Math.PI / 2)
        }
        ctx.translate(-canvas.width / 2, -canvas.height / 2)
        ctx.drawImage(image, 0, 0, width, height);
      }
      let dataUrl = canvas.toDataURL('image/jpeg', 0.3);
      return dataURItoBlob(dataUrl);

    };

    return new Promise((ok, no) => {
      if (!file.type.match(/image.*/)) {
        no(new Error("Not an image"));
        return;
      }

      reader.onload = (readerEvent: any) => {
        image.onload = () => ok(resize());
        image.src = readerEvent.target.result;
      };
      reader.readAsDataURL(file);
    })
  };

  private handleCreateClick = () => {

    let action = this.props.correctionActions[this.state.correctiveAction];// required

    let reqclass = (action && action.value === "Create Work Order") ? this.props.requestClasses[this.state.requestClass] : { _id: null, name: null }; // required if create work order
    // const reqclass = this.props.requestClasses[this.state.requestClass]; // required if create work order
    let responsible = (action && action.value === "User Action Required") ? this.props.responsiblePersons[this.state.responsiblePerson] : { _id: null, name: null, org: null }; // required if user action required or corrected on site
    // const responsible = this.props.responsiblePersons[this.state.responsiblePerson];
    let risk = this.props.riskLevels[this.state.risk]; // required
    let lab = this.props.labs[this.state.lab]; // required
    let org = (action && action.value === "Corrected On-site") ? this.props.organizations[this.state.organization] : { _id: null, name: null }; // required if corrected on-site (we will default based on the responsible persons org)
    // do we need this part? aren't we already giving null default values above?
    if (this.state.flagged) {
      if (!action) {
        action = { _id: null, value: null }
      }
      if (!lab) {
        lab = { _id: null, name: null }
      }
      if (!reqclass) {
        reqclass = { _id: null, name: null }
      }
      if (!org) {
        org = { _id: null, name: null }
      }
      if (!responsible) {
        responsible = { _id: null, name: null, org: null };
      }
      if (!risk) {
        risk = { _id: null, value: null }
      }
    }
    const uuid = (function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return Date.now().toString() + '-' + s4() + '-' + s4() + '-' + s4();
    })();

    let workTask: Model.NewWorkTask =
    {
      responsible: { id: responsible._id, value: responsible.name },
      action: { id: action._id, value: action.value },
      risk: { id: risk._id, value: risk.value },
      org: { id: org._id, value: org.name },
      num: 0, // fixme
      name: 'Lab Audit Observation',
      bench: this.state.bench,
      description: this.state.description,
      flagged: this.state.flagged,
      space: { id: lab._id, value: lab.name },
      reqclass: { id: reqclass._id, value: reqclass.name },
      auditedOrgs: this.state.auditedDepartments,
      id: uuid,
      copy: false
    };
    this.props.onCreate(this.props.question, workTask, this.state.images);

  }
  private deleteImage = () => {
    // if (confirm("Are you sure you want to delete this picture?")) {
    if (this.state.images) {
      const images = this.state.images.slice()
      images.splice(this.state.deleteIndex, 1);

      this.setState({ images, deleteIndex: -1 });
    }

    // }
  }
  private handleDialogOpen = (type: string, index?: number) => () => {
    let msg = "";

    switch (type) {
      case 'deleteImg':
        msg = "Are you sure you want to delete the image?"
        break;
      case 'retire':
        msg = "Are you sure you want to retire the inspection?"
        break;
      case 'cancelCreate':
        msg = "Are you sure you want to cancel this observation?"
        break;
      case 'overrideImg':
        msg = "Your third image will be overwritten. Are you sure you want to continue?";
        break;
    }
    let stateObj = {}
    if (index) {
      stateObj = { deleteIndex: index }
    }
    this.setState({ ...stateObj, openDialog: true, dialogType: type, dialogMessage: msg });
  }
  private handleDialogCloseClick = () => {
    switch (this.state.dialogType) {
      case 'deleteImg':
        this.deleteImage()
        break;
      case 'overrideImg':
        this.confirmTakePicture()

    }
    this.setState({ openDialog: false })
  }

  private handleDialogClose = () => {
    this.setState({ openDialog: false });
  }

  private confirmTakePicture = () => {
    if (this.pictureInput) {
      this.pictureInput.click();
    }
  }

  private canCreateObservation = () => {
    const bench = this.state.bench;
    const observation = this.state.description
    const action = this.props.correctionActions[this.state.correctiveAction];// required
    const reqclass = this.props.requestClasses[this.state.requestClass]; // required if create work order
    const responsible = this.props.responsiblePersons[this.state.responsiblePerson]; // required if user action required or corrected on site
    const risk = this.props.riskLevels[this.state.risk]; // required
    const lab = this.props.labs[this.state.lab]; // required
    const org = this.props.organizations[this.state.organization]; // required if corrected on-site (we will default based on the responsible persons org)
    // const auditedOrgs = this.props.organizations[this.state.organization]; // required
    if (this.state.flagged) {
      return true
    } else if (action && this.state.auditedDepartments.length > 0 && risk && lab && bench.length > 0 && observation.length > 0) {
      if (action.value === "Create Work Order" && reqclass) {
        return true;
      }
      else if (action.value === "User Action Required" && responsible) {
        return true
      }
      else if (action.value === "Corrected On-site" && org) {
        return true
      }
    }
    return false;

  }

  private handleFlaggedChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {

    this.setState({ flagged: e.target.checked });
  }

  private handleOrganizationChange = (value: any) => {
    if (value) {

      this.setState({ organization: value.value });
    } else {
      this.setState({ organization: -1 });
    }
  }

  private handleLabChange = (e: any) => {
    if (e) {
      const _index = this.props.organizations.findIndex((org) => {
        let array = org.space as string[]
        for (let space of array) {
          const boolean = (space === this.props.labs[Number(e.value)].name);
          if (boolean) {
            return true
          }
        }
        return false
      });
      let _matchingOrg = {};
      if (_index !== -1) {
        const _name = this.props.organizations[_index].name;
        const _value = _index.toString()
        const _id = this.props.organizations[_index]._id
        const _space = this.props.organizations[_index].space
        _matchingOrg = { name: _name, value: _value, _id: _id, space: _space }
      }
      const _stateIndex = this.state.auditedDepartments.findIndex(org => JSON.stringify(org) === JSON.stringify(_matchingOrg));
      const _orgArray = Object.keys(_matchingOrg).length > 0 && _stateIndex === -1 ? [...this.state.auditedDepartments, _matchingOrg] as Model.Organization[] : this.state.auditedDepartments;


      this.setState({ lab: Number(e.value), auditedDepartments: _orgArray });
    }
    else {
      this.setState({ lab: -1 });
    }

  }

  private handleRiskChange = (e: React.MouseEvent<any>) => {

    e.stopPropagation();
    e.preventDefault();

    const target = e.target as HTMLElement;
    const text = target.textContent
    const _riskIndex = this.props.riskLevels.findIndex(risk => risk.value === text);

    this.setState({ risk: _riskIndex });
  }

  private handleCorrectiveActionChange = (e: any) => {
    if (e) {
      let _obj = {}
      if (Number(e.value) === 0) {
        _obj = { organization: -1, requestClass: -1 }
      } else if (Number(e.value) === 2) {
        _obj = { organization: -1, responsiblePerson: -1, }
      }
      else if (Number(e.value) === 1) {
        _obj = { reqclass: -1, responsiblePerson: -1 }
      }
      this.setState({ correctiveAction: Number(e.value), ..._obj });

    }
    else {
      this.setState({ correctiveAction: -1 });
    }
  }



  private handleResponsiblePersonChange = (value: any) => {

    if (value) {
      this.setState({ responsiblePerson: value.value });
    } else {
      this.setState({ responsiblePerson: -1 });
    }
  }

  private handleRequestClassChange = (value: any) => {

    if (value) {
      this.setState({ requestClass: value.value });
    } else {
      this.setState({ requestClass: -1 });
    }

  }

  private handleBenchChange = () => {
    const _bench = this.benchInput.value
    this.setState({ bench: _bench });
  }

  private handleDescriptionChange = () => {
    this.setState({ description: this.descriptionInput.value })
  }

  private clearDescriptionHeight = () => {
    if (this.state.descriptionHeight > 0) {
      this.setState({ descriptionHeight: 0 })
    }
  }

  private getTextWidth(text: any, font: any) {
    const context = this.textCanvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
  }
  private setDescriptionRows = () => {
    const _value = this.descriptionInput.value ? this.descriptionInput.value : 'tjene';
    const _valueWidth = this.getTextWidth(_value, 'normal 14px arial');
    const width = Math.ceil((_valueWidth + 50) / this.descriptionInput.offsetWidth);
    let _rows = 21;
    if (width >= 2) {
      _rows += 11;
    }
    if (width >= 3) {
      _rows += ((width - 2) * 16)
    }

    this.setState({ descriptionHeight: _rows })
  }
  private handleDescriptionSelect = (value: any) => {
    this.descriptionInput.value = value.name;
    this.setDescriptionRows();
    this.setState({ description: value.name })
  }
  private handleAuditedDepartmentsChange = (value: any) => {
    this.setState({ auditedDepartments: value });
  }

  private cameraClick = (event: React.MouseEvent<SVGElement>) => {
    if (this.pictureInput) {
      if (this.state.images && this.state.images.length >= 3) {
        this.handleDialogOpen('overrideImg')()

      }
      else {
        this.pictureInput.click();

      }
    }

  }

  private takePicture = (e: React.ChangeEvent<HTMLInputElement>) => {

    const input = e.target;
    const reader = new FileReader();

    if (input.files && input.files[0]) {
      this.resizeImage({ file: input.files[0], maxSize: 500 })
        .then((res: any) => {
          reader.onload = (e: any) => {
            if (this.pictureInput && this.image && e.target) {

              //this.image.src = e.target.result;
              let _imageArr: any = []
              if (this.state.images) {
                _imageArr = this.state.images.slice();

              }
              if (_imageArr.length === 3 && input.files && input.files[0]) {
                _imageArr[2] = { imageURL: e.target.result };
              } else if (input.files && input.files[0]) {
                _imageArr[_imageArr.length] = { imageURL: e.target.result };

              }
              this.setState({ images: _imageArr, })
              this.pictureInput.value = '';
            }
          }
          reader.readAsArrayBuffer(res);
        })

    }
  }


  private blobUrl = (blob: any) => {
    if (this.props.urls.has(blob)) {
      return this.props.urls.get(blob)
    } else {
      let url = URL.createObjectURL(blob)
      this.props.urls.set(blob, url)
      return url
    }
  }
  private getImages = () => {
    let _imageArr: any = [];
    for (let i = 0; i < 3; i++) {
      if (this.state.images && this.state.images[i]) {
        const dataView = new DataView(this.state.images[i].imageURL);
        const blob = new Blob([dataView], { type: 'image/jpeg' });
        const imageUrl = this.blobUrl(blob);
        _imageArr.push(<div key={i} style={{ display: 'inline-flex', width: 110, height: 110, position: 'relative' }} ><img onClick={this.props.imageClick} src={imageUrl} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
          <IconButton disableRipple style={{ color: 'red', position: 'absolute', right: '0' }} onClick={this.handleDialogOpen('deleteImg', i)}>
            <svg width="25px" height="25px" viewBox="0 0 20 20" version="1.1">
              <title>Delete Image</title>
              <desc>Created with Sketch.</desc>
              <defs></defs>
              <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Group-17" transform="translate(-2.000000, -2.000000)">
                  <circle id="Oval-2" fill="#FFFFFF" fillRule="nonzero" cx="11.5" cy="12.5" r="7.5"></circle>
                  <g id="ic_cancel_24px">
                    <g id="Group">
                      <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                      <path d="M12,2 C6.47,2 2,6.47 2,12 C2,17.53 6.47,22 12,22 C17.53,22 22,17.53 22,12 C22,6.47 17.53,2 12,2 L12,2 Z M17,15.59 L15.59,17 L12,13.41 L8.41,17 L7,15.59 L10.59,12 L7,8.41 L8.41,7 L12,10.59 L15.59,7 L17,8.41 L13.41,12 L17,15.59 L17,15.59 Z" id="Shape" fill="#FF0000"></path>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
          </IconButton>
        </div>);
      } else {
        _imageArr.push(<div key={i}>
          <svg width="100px" height="100px" viewBox="0 0 101 101" version="1.1">
            <title>Image Placeholder</title>
            <desc>Created with Sketch.</desc>
            <defs></defs>
            <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
              <g id="Group-16" transform="translate(2.000000, 2.000000)" strokeWidth="3">
                <g id="Page-1-Copy-16" stroke="#A6A8AB">
                  <polyline id="Stroke-1" points="97 90 97 97 90 97"></polyline>
                  <path d="M78,97 L13,97" id="Stroke-3" strokeDasharray="10.142,10.142"></path>
                  <polyline id="Stroke-5" points="7 97 0 97 0 90"></polyline>
                  <path d="M0,78 L0,13" id="Stroke-7" strokeDasharray="10.142,10.142"></path>
                  <polyline id="Stroke-9" points="0 7 0 0 7 0"></polyline>
                  <path d="M19,0 L84,0" id="Stroke-11" strokeDasharray="10.142,10.142"></path>
                  <polyline id="Stroke-13" points="90 0 97 0 97 7"></polyline>
                  <path d="M97,19 L97,84" id="Stroke-15" strokeDasharray="10.142,10.142"></path>
                </g>
                <g id="image-copy-20" transform="translate(27.000000, 25.000000)" stroke="#A4A8AB" strokeLinecap="round" strokeLinejoin="round">
                  <rect id="Rectangle-path" x="0" y="0" width="44" height="44" rx="2"></rect>
                  <circle id="Oval" cx="13.5" cy="13.5" r="3.5"></circle>
                  <polyline id="Shape" points="44 29.2727273 31.8125 17 5 44"></polyline>
                </g>
              </g>
            </g>
          </svg>
        </div>)
      }
    }
    return _imageArr;
  }

  private setRiskStyle = (option: any): React.CSSProperties | undefined => {
    let _style = { margin: '6px 0px', flexGrow: 1, boxShadow: 'none', border: '#ccc solid 1px', borderRadius: 0 } as React.CSSProperties;
    if (this.state.risk === option) {

      if (option === 0) {
        _style.backgroundColor = '#ffff80'
      } else if (option === 1) {
        _style.backgroundColor = '#ffa64d'
      } else if (option === 2) {
        _style.backgroundColor = '#ff704d'
      }
      _style.fontWeight = 'bold'

    } else {
      _style.backgroundColor = 'white'
    }
    return _style
  }
  render() {
    return <div style={{ marginBottom: 20, padding: 15, overflow: 'hidden', border: '1px solid grey ', backgroundColor: 'white', WebkitOverflowScrolling: 'touch' }}>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '30%', verticalAlign: 'middle' }} id="questionBox">
          <div>
            {this.props.question.cat}
          </div>
          <div>
            {this.props.question.text}
          </div>
        </div>
        <div style={{ flexGrow: 1, display: 'inline-flex', justifyContent: 'space-evenly', alignItems: 'center' }}>
          {this.getImages()}
        </div>


        <div style={{ alignContent: 'center', alignSelf: 'center', boxSizing: 'border-box', display: 'inline-flex', borderRadius: 20, boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)' }}>
          <svg style={{ borderRadius: 20 }} onClick={this.cameraClick} width="125px" height="125px" viewBox="0 0 157 157" version="1.1">
            <title>Group 9</title>
            <desc>Created with Sketch.</desc>
            <defs></defs>
            <g id="LabAudit-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
              <g id="Group-9">
                <g id="Group-2-Copy-9" fill="#FFFFFF">
                  <rect id="Rectangle" x="0" y="0" width="157" height="157" rx="15"></rect>
                </g>
                <g id="camera-copy-8" transform="translate(38.000000, 45.000000)" stroke="#314474" strokeLinecap="round" strokeLinejoin="round" strokeWidth="6">
                  <path d="M82,59.5555556 C82,63.6670087 78.6624863,67 74.5454545,67 L7.45454545,67 C3.33751368,67 8.276208e-16,63.6670087 0,59.5555556 L0,18.6111111 C-4.138104e-16,14.499658 3.33751368,11.1666667 7.45454545,11.1666667 L22.3636364,11.1666667 L29.8181818,0 L52.1818182,0 L59.6363636,11.1666667 L74.5454545,11.1666667 C78.6624863,11.1666667 82,14.499658 82,18.6111111 L82,59.5555556 Z" id="Shape"></path>
                  <circle id="Oval" cx="41" cy="37" r="15"></circle>
                </g>
              </g>
            </g>
          </svg>
          <img style={{ visibility: 'hidden', height: 1, width: 1, top: -10 }} ref={e => this.image = e} />
          <input style={{ visibility: 'hidden', position: 'absolute' }} onChange={this.takePicture} ref={e => this.pictureInput = e} type="file" name="myImage" accept="image/x-png,image/gif,image/jpeg,image/png,image/jpg" />
        </div>

      </div>
      <div >

        <FormControlLabel style={{ marginLeft: '-3px' }}
          control={<Checkbox style={{ width: '25px', color: 'rgb(225,0,80)' }} onChange={this.handleFlaggedChange} checked={this.state.flagged}>Flagged</Checkbox>}
          label="Flagged for review">
        </FormControlLabel>
        <InputLabel style={{ display: 'block', fontSize: '10px' }}>Risk Level</InputLabel>
        <div style={{ marginTop: '10px', display: 'flex' }}>{this.props.riskLevels.map((option, optionIndex) => {
          return <ButtonMaterial style={this.setRiskStyle(optionIndex)} disableRipple onClick={this.handleRiskChange} key={optionIndex} variant="raised" >{option.value}</ButtonMaterial>
        })}
        </div>
        <FormControl style={{ marginBottom: 20, marginTop: 10 }} fullWidth>
          <InputLabel shrink={true} htmlFor="labInput">Select Lab</InputLabel>
          <Select
            style={{ marginTop: 16 }}
            searchable
            id="labInput"
            placeholder="Select a lab"
            closeOnSelect={true}
            options={this.props.labs.map((object, i) => ({
              name: object.name, value: i.toString(), _id: object._id
            }))}
            onChange={this.handleLabChange}
            //@ts-ignore
            value={this.state.lab}
            labelKey="name" />
        </FormControl>
        <FormControl style={{ marginBottom: 20 }} fullWidth>
          <InputLabel shrink={true} htmlFor="multiOrgSelect" >Pick Audited Department(s)</InputLabel>
          <Select
            style={{ marginTop: 16 }}
            searchable
            id="multiOrgSelect"
            placeholder="Select One or more Department"
            closeOnSelect={false}
            multi={true}
            options={this.props.organizations.map((object, i) => ({
              name: object.name, value: i.toString(), _id: object._id, space: object.space
            }))}
            onChange={this.handleAuditedDepartmentsChange}
            //@ts-ignore
            value={this.state.auditedDepartments}
            labelKey="name" />
        </FormControl>
        <FormControl fullWidth style={{ marginBottom: 20 }}>
          <InputLabel shrink={true} htmlFor="benchInput" >Bench/Location</InputLabel>
          <Input
            disableUnderline
            multiline
            placeholder="Enter specific Bench/Location"
            inputProps={{ id: 'benchInput', maxLength: 1000 }}
            defaultValue=""
            onBlur={this.handleBenchChange}
            inputRef={(e) => { this.benchInput = e }}
            style={{ textAlign: 'center', border: 'lightgrey 1px solid', borderRadius: 4, paddingLeft: 5, paddingRight: 5 }}></Input>
        </FormControl>
        <FormControl fullWidth style={{ marginBottom: 20 }}>
          <InputLabel shrink={true} htmlFor="descriptionInput" >Observation/Action</InputLabel>
          <div className="textareaDiv" style={{ marginTop: 15 }}>
            <Select
              style={{ position: 'absolute' }}
              id="descriptionInput"
              closeOnSelect={true}
              options={this.state.preCannedResponses}
              searchable={false}
              labelKey="name"
              onChange={this.handleDescriptionSelect}
              //@ts-ignore
              value="" />
            <Input
              disableUnderline
              multiline
              inputProps={{ id: 'descriptionInput', maxLength: 1000, style: this.state.descriptionHeight > 0 ? { height: this.state.descriptionHeight } : {} }}
              defaultValue={this.state.description}
              onBlur={this.handleDescriptionChange}
              onChange={this.clearDescriptionHeight}
              placeholder="Type an observation or select a pre-defined one (When applicable)"
              inputRef={(e) => { this.descriptionInput = e }}
              style={{ textAlign: 'center', borderRadius: '4px 0px 0px 4px', paddingLeft: 5, position: 'relative', paddingRight: 5, width: 'calc(100% - 28px)', top: 0, minHeight: this.state.descriptionHeight > 0 ? this.state.descriptionHeight + 15 : 36, boxSizing: 'border-box', border: '1px lightgrey solid', backgroundColor: 'white' }}></Input>
          </div>
        </FormControl>
        <FormControl style={{ marginBottom: 20 }} fullWidth>
          <InputLabel shrink={true} htmlFor="userActionInput">Corrective Action</InputLabel>
          <Select
            style={{ marginTop: 16 }}
            id="userActionInput"
            clearable={false}
            searchable={false}
            placeholder="Choose Corrective Action"
            closeOnSelect={true}
            options={this.props.correctionActions.map((ca, i) => ({
              label: ca.value as string,
              value: i
            }))}
            onChange={this.handleCorrectiveActionChange}
            //@ts-ignore
            value={this.state.correctiveAction}
          />

        </FormControl>
        {this.state.correctiveAction > -1 && this.props.correctionActions[this.state.correctiveAction].value === 'User Action Required' &&
          <FormControl style={{ marginBottom: 20 }} fullWidth>
            <InputLabel shrink={true} htmlFor="rpSelect" >Responsible Person</InputLabel>
            <Select
              style={{ marginTop: 16 }}
              searchable
              id="rpSelect"
              placeholder="Type to search for a LSC"
              closeOnSelect={true}
              options={this.props.responsiblePersons.map(({ name }, i) => ({
                label: name,
                value: i
              }))}
              onChange={this.handleResponsiblePersonChange}
              //@ts-ignore
              value={this.state.responsiblePerson}
            />
          </FormControl>
        }

        {this.state.correctiveAction > -1 && this.props.correctionActions[this.state.correctiveAction].value === 'Corrected On-site' &&
          <FormControl style={{ marginBottom: 20 }} fullWidth>
            <InputLabel shrink={true} htmlFor="organizationInput">Corrected-by Organization</InputLabel>
            <Select
              style={{ marginTop: 16 }}
              searchable
              id="organizationInput"
              placeholder="Type to search for an Organization"
              closeOnSelect={true}
              options={this.props.organizations.map(({ name }, i) => ({
                label: name,
                value: i
              }))}
              onChange={this.handleOrganizationChange}
              //@ts-ignore
              value={this.state.organization}
            />
          </FormControl>
        }
        {this.state.correctiveAction > -1 && this.props.correctionActions[this.state.correctiveAction].value === 'Create Work Order' &&
          <FormControl style={{ marginBottom: 20 }} fullWidth>
            <InputLabel shrink={true} htmlFor="rcSelect">Request Class</InputLabel>
            <Select
              style={{ marginTop: 16 }}
              searchable
              id="rcSelect"
              placeholder="Type to search for request class"
              closeOnSelect={true}
              options={this.props.requestClasses.map(({ parent, name }, i) => ({
                label: parent ? `${parent}/${name}` : name,
                value: i
              }))}
              onChange={this.handleRequestClassChange}
              //@ts-ignore
              value={this.state.requestClass}
              menuStyle={{ height: 100 }}
            />
          </FormControl>
        }
        <React.Fragment>
          <ButtonMaterial disableRipple style={{ float: 'right', backgroundColor: 'rgb(191, 40, 40)', color: 'white', marginLeft: 10 }} variant="raised" onClick={this.props.onCancel}>Cancel</ButtonMaterial>
          <ButtonMaterial disableRipple style={{ float: 'right', color: 'white' }} color="primary" variant="raised" disabled={!this.canCreateObservation()} onClick={this.handleCreateClick}>Create</ButtonMaterial>
        </React.Fragment>
        <Dialog open={this.state.openDialog} onClose={this.handleDialogClose}>
          <DialogTitle>
            {this.state.dialogMessage}
          </DialogTitle>
          <DialogActions>
            <ButtonMaterial variant="outlined" onClick={this.handleDialogClose} disableRipple>Cancel</ButtonMaterial>
            <ButtonMaterial variant="raised" onClick={this.handleDialogCloseClick} disableRipple>Confirm</ButtonMaterial>

          </DialogActions>
        </Dialog>

      </div>
    </div>;
  }
}
export default ObservationCreator;