export interface Organization {
  id: string;
  name: string;
  space: string | string[];
  _id: string;
}

export interface RequestClass {
  _id: string;
  name: string;
  parent: string;
}

export interface Floor {
  _id: string;
  name: string;
}

export interface Building {
  _id: string;
  name: string;
}

export interface User {
  firstName: string;
  lastName: string;
}

export interface RiskLevel {
  _id: string | null;
  value: string | null;
}

export interface WorkOrderAction {
  _id: string | null;
  value: string | null;
}

export interface ResponsiblePerson {
  _id: string;
  name: string;
  org: string;
}

export interface Category {
  _id: string;
  name: string;
}

export interface Lab {
  _id: string;
  name: string;
  building: {
    id: string;
    value: string;
  };
  floor: {
    id: string;
    value: string;
  };
}

export interface NewInspection {
  building: {
    id: string | null; // Check
    value: string | null; // Check
  };
  comments: string | null;
  date: string | null;
  floor: {
    id: string | null; // Check
    value: string | null; // Check
  };
  inspectionname: string | null;
  multifloor: boolean;
  org: {
    id: string | null; // Check
    value: string | null; // Check
  };
  space: {
    id: string | null; // Check
    value: string | null; // Check
  };
  type: string;
  started: boolean;
}

export interface Inspection extends NewInspection {
  _id: string;
}

export interface Question {
  _id: string;  // Unique identifier.
  type: { id: number; value: 'Yes/No/NA' | string; };
  cat: string;  // Question category.
  text: string; // Human readable question.
  num: number;  // Position of question within its category.
  wtcreated: boolean
  resp: string | null; // The answer.
  comments: string; // Answer comments.
  preCannedResponses?: PreCannedResponse[],
  hasPRC:boolean;
}

export interface PreCannedResponse {
  name: string;
  _id: string;
}

export interface NewWorkTask {
  action: { id: string | null; value: string | null; };
  bench: string;
  description: string;
  flagged: boolean;
  name: string;
  num: number;
  org: {
    id: string | null;
    value: string | null;
  };
  auditedOrgs: Organization[];
  reqclass: { id: string | null; value: string | null; };
  responsible: { id: string | null; value: string | null; };
  risk: { id: string | null; value: string | null; };
  space: { id: string | null; value: string | null; };
  images?: string;
  id: string;
  copy:boolean
}

export interface WorkTask extends NewWorkTask {
  _id: string;
}

export interface Space {
  _id: string | null;
  name: string | null;
}

export interface NewImage {
  imageURL: ArrayBuffer;
  imageSource?: ArrayBuffer;
}

export interface Image extends NewImage {
  _id: string
}

export interface IResizeImageOptions {
  maxSize: number;
  file: File;
}