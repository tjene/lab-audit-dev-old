import * as React from 'react';
import * as Model from './Model';
import * as _localforage from 'localforage';
import ObservationInspector from './ObservationInspector';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import InfiniteScroll from 'react-infinite-scroller';
import SnackBar from '@material-ui/core/Snackbar';
import { isEqual, cloneDeep, debounce } from 'lodash';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonMaterial from '@material-ui/core/Button';





const localforage = (_localforage as any).default as typeof _localforage; //localStorage delcaration

module ObservationsTab {
    export interface Props {
        groupedQuestions: Model.Question[][];
        selectedSpacesById: { [id: string]: Model.Space; };
        riskLevels: Model.RiskLevel[];
        correctionActions: Model.WorkOrderAction[];
        responsiblePersons: Model.ResponsiblePerson[];
        requestClasses: Model.RequestClass[];
        organizations: Model.Organization[];
        groupedObservations: [Model.Question, Model.NewWorkTask][][];
        categories: Model.Category[];
        labs: Model.Space[];
        updateObservations: (observations: any, observation: any) => Promise<any> | undefined;
        deleteObservations: (observations: any, observation: any) => Promise<any> | undefined;
        urls: any;
        inspectionId: any;
        obsPreviewImage: (url: any) => any
    }

    export interface State {
        searchValueObs: string;
        observationCategoryTab: number;
        scrollPage: number;
        hasMoreItems: boolean;
        observationToast: boolean;
        messageContent: string;
        observationTotal: number;
        groupedObservations: [Model.Question, Model.NewWorkTask][][];
        openDialog: boolean
    }
}

class ObservationsTab extends React.Component<ObservationsTab.Props, ObservationsTab.State> {
    state: ObservationsTab.State = {
        searchValueObs: "",
        observationCategoryTab: 0,
        scrollPage: 1,
        hasMoreItems: true,
        observationToast: false,
        messageContent: '',
        observationTotal: 0,
        groupedObservations: [],
        openDialog: false
    }

    componentDidMount() {
        const groupedObservations = this.props.groupedObservations;
        this.setState({ groupedObservations: groupedObservations, scrollPage: 1 })
    }

    componentDidCatch(e: any, info: any) {
        console.log("error", e);
        console.log("info", info)
    }
  
    shouldComponentUpdate(nextProps: any, nextState: any) {
        if (isEqual(nextProps, this.props) && isEqual(nextState, this.state)) {
            return false;
        } else {
            return true;
        }
    }

    private searchInput: any = null
    //private image: HTMLImageElement | null = null;
    private lastY: any = 0;
    private textCanvas: any = document.createElement("canvas");
    debounceInput: any = debounce((event: any) => {
        this.setState({ searchValueObs: event.target.value })
    }, 301);
    debounceBench: any = debounce((e: any, id: any, catId: any, question: any) => {
        if (e.target.value === "") {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                bench: e.target.value, flagged: true
            });
        }
        else {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                bench: e.target.value
            });
        }

    }, 999);
    debounceDesc: any = debounce((e: any, id: any, catId: any, question: any) => {
        if (e.target.value === "") {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                description: e.target.value, flagged: true
            });
        }
        else {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                description: e.target.value
            });
        }
    }, 500);

    componentDidUpdate(prevProps: any, prevState: any) {
        let observationTotal = 0;
        for (let group of this.props.groupedObservations) {
            observationTotal += group.length;
        }
        if (prevState.observationTotal !== observationTotal) {
            let observationTotal = 0;
            for (let group of this.props.groupedObservations) {
                observationTotal += group.length;
            }
            let hasMoreItems;
            if (this.state.observationCategoryTab === 0) {
                hasMoreItems = this.state.scrollPage * 5 < observationTotal;
            } else {
                hasMoreItems = this.state.scrollPage * 5 < this.props.groupedObservations[this.state.observationCategoryTab - 1].length;
            }
            this.setState({ observationTotal, hasMoreItems });
        }
        this.setState({ groupedObservations: this.props.groupedObservations })

    }

    private getToggleSectionStyleObservations = (group: [Model.Question, Model.NewWorkTask][]): React.CSSProperties => {
        if (group) {
            for (let observation of group) {
                if (observation["1"].flagged) {
                    return { color: 'rgb(106,156,224)', fill: 'rgb(106,156,224)' };
                }
            }
        }

        return {};
    }
    private getFlaggedObservationStyle = (observation: Model.NewWorkTask): React.CSSProperties => {

        if (observation.flagged) {
            return { color: 'rgb(106,156,224)', fill: 'rgb(106,156,224)' };
        }
        return {};
    }
    private getToggleAllObservationStyle = (allObservations: [Model.Question, Model.NewWorkTask][][]): React.CSSProperties => {
        for (let group of allObservations) {
            for (let observation of group) {
                if (observation["1"].flagged) {
                    return { color: 'rgb(106,156,224)', fill: 'rgb(106,156,224)' };
                }
            }
        }
        return {};

    }

    private clearSearch = (event: any) => {
        this.searchInput.value = '';
        this.setState({ searchValueObs: '' })
    }

    private handleSearchInputObservation = (event: any) => {
        event.persist()
        if (this.state.observationCategoryTab !== 0) {
            this.setState({ observationCategoryTab: 0 });
        }
        this.debounceInput(event)
    }
    private handleObservationTabChange = (event: React.ChangeEvent<{}>, tab: number) => {
        if (this.state.searchValueObs !== '') {
            this.setState({ searchValueObs: '' });
            this.searchInput.value = '';
        }
        const scrollDiv: any = document.getElementById('scrollDiv');
        scrollDiv.scrollTop = 0;
        this.setState({ observationCategoryTab: tab, scrollPage: 1, hasMoreItems: true });
    }

    private handleObservationChange = (id: number, catId: number, question: Model.Question, task: Model.NewWorkTask) => {
        const groupedObservations = cloneDeep(this.state.groupedObservations)
        for (let i = 0; i < groupedObservations.length; i++) {
            for (let j = 0; j < groupedObservations[i].length; j++) {
                if (id === j && catId === i) {
                    groupedObservations[i][j] = [question, task];
                }
            }
        }
        this.setState({ groupedObservations })
        return this.props.updateObservations(groupedObservations, [question, task])

    }

    private handleObservationDelete = (observationId: number, categoryId: number) => {
        // if (confirm("Are you sure you want to delete the observation?")) {
        let groupedObservations = cloneDeep(this.props.groupedObservations)
        let observation: any = [];
        observation = groupedObservations[categoryId].splice(observationId, 1);
        const _promise = this.props.deleteObservations(groupedObservations, observation[0])

        if (_promise) {
            _promise.then(() => {
                this.setState({ observationToast: true, messageContent: "Observation Deleted" })
            })
        }
        // }
    }

    private handleImagesChange = (images: any, id: number, catId: number, question: Model.Question, task: Model.NewWorkTask) => {
        const _id = `inspection-${this.props.inspectionId}-observations-${task.id}-images`;
        localforage.setItem(_id, images)
            .then(() => {
                this.handleObservationChange(id, catId, question, {
                    ...task, images: _id
                });
            })

    }

    private handleImageDelete = (index: any, id: any, catId: any, question: any, observation: any) => {
        const _id = `inspection-${this.props.inspectionId}-observations-${observation.id}-images`;
        return localforage.getItem(_id)
            .then((res: any) => {
                res.splice(index, 1);
                if (res.length === 0) {
                    const _promise = localforage.setItem(_id, res)
                    if (_promise) {
                        _promise.then(() => {
                            return this.handleObservationChange(id, catId, question, {
                                ...observation, images: null
                            })
                        })
                    }
                    return
                } else {
                    return localforage.setItem(_id, res)
                }

            })
    }

    private handleObservationCopy = async (id: number, question: Model.Question, task: Model.NewWorkTask) => {
        const copyTask = cloneDeep(task);
        const addedObservation = [question, copyTask] as [Model.Question, Model.NewWorkTask]
        const newId = (function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return Date.now().toString() + '-' + s4() + '-' + s4() + '-' + s4();
        })();
        addedObservation[1] = { ...addedObservation[1], id: newId, copy: true, flagged: true, bench: '' }
        if (task.images) {
            let _image = await localforage.getItem(task.images) as any[];
            const _id = `inspection-${this.props.inspectionId}-observations-${newId}-images`;
            const _imageClone = [] as any[];
            for (let i = 0; i < _image.length; i++) {
                _imageClone.push({ imageURL: _image[i].imageURL.slice(0) })
            }
            await localforage.setItem(_id, _imageClone)
            addedObservation[1] = { ...addedObservation[1], images: _id }
        }
        let _index = this.props.categories.findIndex((cat) => {
            if (cat.name === question.cat) {
                return true;
            }
            return false;
        });
        const observations = cloneDeep(this.props.groupedObservations);
        observations[_index].splice((id + 1), 0, addedObservation)
        // observations[_index].push(addedObservation)
        const _promise = this.props.updateObservations(observations, addedObservation)

        if (_promise) {
            _promise.then(() => {
                this.setState({ observationToast: true, messageContent: "Observation Copied" })
            })
        }

    }
    private handleFlaggedChange = (id: number, catId: number, question: any) => (e: any) => {
        let _allowChange = true;
        let _message = ""
        if (this.props.groupedObservations[catId][id][1].flagged) {
            if (!this.props.groupedObservations[catId][id][1].risk.value || this.props.groupedObservations[catId][id][1].auditedOrgs.length === 0 || !this.props.groupedObservations[catId][id][1].space.value || !this.props.groupedObservations[catId][id][1].bench || !this.props.groupedObservations[catId][id][1].description || !this.props.groupedObservations[catId][id][1].action.value) {
                _allowChange = false
            } else if (this.props.groupedObservations[catId][id][1].action.value === "Create Work Order" && !this.props.groupedObservations[catId][id][1].reqclass.value) {
                _allowChange = false;
            }
            else if (this.props.groupedObservations[catId][id][1].action.value === "User Action Required" && !this.props.groupedObservations[catId][id][1].responsible.value) {
                _allowChange = false
            }
            else if (this.props.groupedObservations[catId][id][1].action.value === "Corrected On-site" && !this.props.groupedObservations[catId][id][1].org.value) {
                _allowChange = false
            }
        }
        if (_allowChange) {
            const workTask = this.props.groupedObservations[catId][id][1] as Model.NewWorkTask
            this.handleObservationChange(id, catId, question, {
                ...workTask, flagged: e.target.checked
            });
        } else {
            // alert("Please complete all fields before unflagging observation.") 
            this.setState({ openDialog: true });
        }

    }
    private handleAlertDialogClose = () => {
        this.setState({ openDialog: false })
    }
    private handleOrganizationChange = (id: number, catId: number, question: any) => (e: any) => {

        if (e) {
            const _organization = this.props.organizations[Number(e.value)]
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                org: { id: _organization._id, value: _organization.name }
            });
        } else {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                org: { id: null, value: null }, flagged: true
            });
        }
    }
    private handleLabChange = (id: number, catId: number, question: any) => (e: any) => {
        const _index = this.props.organizations.findIndex((org) => {
            let array = org.space as string[]
            for (let space of array) {
                const boolean = (space === this.props.labs[Number(e.value)].name);
                if (boolean) {
                    return true
                }
            }
            return false
        });
        let _matchingOrg = {};
        if (_index !== -1) {
            const _name = this.props.organizations[_index].name;
            const _value = _index.toString()
            const _id = this.props.organizations[_index]._id
            const _space = this.props.organizations[_index].space
            _matchingOrg = { name: _name, value: _value, _id: _id, space: _space }
        }
        const _stateIndex = this.props.groupedObservations[catId][id][1].auditedOrgs.findIndex(org => JSON.stringify(org) === JSON.stringify(_matchingOrg));
        const _orgArray = Object.keys(_matchingOrg).length > 0 && _stateIndex === -1 ? [...this.props.groupedObservations[catId][id][1].auditedOrgs, _matchingOrg] as Model.Organization[] : this.props.groupedObservations[catId][id][1].auditedOrgs;
        const _lab = this.props.labs[Number(e.value)]
        this.handleObservationChange(id, catId, question, {
            ...this.props.groupedObservations[catId][id][1],
            space: { id: _lab._id, value: _lab.name },
            auditedOrgs: _orgArray
        });

    }
    private handleRiskChange = (id: number, catId: number, question: any) => (e: any) => {

        e.stopPropagation();
        e.preventDefault();

        const target = e.target as HTMLElement;
        const text = target.textContent
        const _riskIndex = this.props.riskLevels.findIndex(risk => risk.value === text);

        const _risk = this.props.riskLevels[_riskIndex]
        this.handleObservationChange(id, catId, question, {
            ...this.props.groupedObservations[catId][id][1],
            risk: { id: _risk._id, value: _risk.value }
        });

    }
    private handleCorrectiveActionChange = (id: number, catId: number, question: any) => (e: any) => {
        let _obj = {}
        if (Number(e.value) === 0) {
            _obj = { organization: -1, requestClass: -1 }
        } else if (Number(e.value) === 2) {
            _obj = { organization: -1, responsiblePerson: -1, }
        }
        else if (Number(e.value) === 1) {
            _obj = { reqclass: -1 }
        }
        if (id !== -1 && catId !== -1 && this.props.groupedObservations[catId][id][1]) {
            const _correctionAction = this.props.correctionActions[Number(e.value)]
            if (Number(e.value) === 0 && _correctionAction.value && _correctionAction._id) { //user action
                this.handleObservationChange(id, catId, question, {
                    ...this.props.groupedObservations[catId][id][1],
                    action: { id: _correctionAction._id, value: _correctionAction.value },
                    org: { id: null, value: null },
                    reqclass: { id: null, value: null },
                    flagged: true
                });
            }
            else if (Number(e.value) === 2 && _correctionAction.value && _correctionAction._id) { //corrected onsite
                this.handleObservationChange(id, catId, question, {
                    ...this.props.groupedObservations[catId][id][1],
                    action: { id: _correctionAction._id, value: _correctionAction.value },
                    reqclass: { id: null, value: null },
                    flagged: true,
                    responsible: { id: null, value: null }
                });
            }
            else if (Number(e.value) === 1 && _correctionAction.value && _correctionAction._id) { //work order
                this.handleObservationChange(id, catId, question, {
                    ...this.props.groupedObservations[catId][id][1],
                    action: { id: _correctionAction._id, value: _correctionAction.value },
                    org: { id: null, value: null },
                    responsible: { id: null, value: null },
                    flagged: true
                });
            }

        }


    }
    private handleResponsiblePersonChange = (id: number, catId: number, question: any) => (e: any) => {

        if (e) {
            const _responsiblePerson = this.props.responsiblePersons[Number(e.value)]
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                responsible: { id: _responsiblePerson._id, value: _responsiblePerson.name }
            });
        }
        else {
            this.handleObservationChange(id, catId, question, { ...this.props.groupedObservations[catId][id][1], responsible: { id: null, value: null }, flagged: true });
        }
    }

    private handleRequestClassChange = (id: number, catId: number, question: any) => (e: any) => {

        if (e) {
            const _catId = catId
            const _reqClass = this.props.requestClasses[Number(e.value)]
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                reqclass: { id: _reqClass._id, value: _reqClass.name }
            });
        }
        else {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                reqclass: { id: null, value: null }, flagged: true
            });
        }

    }

    private handleBenchChange = (id: number, catId: number, question: any) => (e: any) => {
        e.persist();
        this.debounceBench(e, id, catId, question);
    }

    private handleDescriptionChange = (e: any, id: number, catId: number, question: any) => {
        e.persist()
        this.debounceDesc(e, id, catId, question);
    }

    private handleDescriptionSelect = (value: any, id: number, catId: number, question: any) => {
        this.handleObservationChange(id, catId, question, {
            ...this.props.groupedObservations[catId][id][1],
            description: value
        });
    }

    private getTextWidth(text: any, font: any) {
        const context = this.textCanvas.getContext("2d");
        context.font = font;
        const metrics = context.measureText(text);
        return metrics.width;
    }

    private setDescriptionRows = (value: any, width: number) => {
        const _value = value ? value : 'tjene';
        //const _rows = 32 + ((Math.ceil((this.getTextWidth(_value, 'normal 14px arial') + 50) / width) - 2) * 16)
        const _width = Math.ceil((this.getTextWidth(_value, 'normal 14px arial') + 50) / width);
        let _rows = 21;
        if (_width >= 2) {
            _rows += 11;
        }
        if (_width >= 3) {
            _rows += ((_width - 2) * 16)
        }
        return _rows;
    }

    private handleAuditedDepartmentsChange = (id: number, catId: number, question: any) => (value: any) => {
        if (value.length === 0) {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                auditedOrgs: value, flagged: true
            });
        }
        else {
            this.handleObservationChange(id, catId, question, {
                ...this.props.groupedObservations[catId][id][1],
                auditedOrgs: value
            });
        }



    }

    private handleObservationContinue = (id: any, catId: any, question: any, task: any) => {
        const _observation = this.props.groupedObservations[catId][id][1]
        let _obs = { ...this.props.groupedObservations[catId][id][1], copy: false }
        if (_observation.risk.value && _observation.auditedOrgs.length > 0 && _observation.space.value && _observation.bench && _observation.description && _observation.action.value) {
            if (_observation.action.value === "Create Work Order" && _observation.reqclass.value || _observation.action.value === "User Action Required" && _observation.responsible.value || _observation.action.value === "Corrected On-site" && _observation.org.value) {
                _obs.flagged = false
            }
        }
        this.handleObservationChange(id, catId, question, _obs)
    }

    private loadScroll = () => {
        if (this.props.groupedObservations.length > 0) {
            let scrollPage = this.state.scrollPage + 1;
            if (this.state.observationCategoryTab === 0) {
                if (scrollPage * 5 >= this.state.observationTotal) {
                    this.setState({ scrollPage, hasMoreItems: false })
                } else {
                    this.setState({ scrollPage })
                }
            }
            else if (scrollPage * 5 >= this.props.groupedObservations[this.state.observationCategoryTab - 1].length) {
                this.setState({ scrollPage, hasMoreItems: false })
            } else {
                this.setState({ scrollPage });
            }
        }


    }

    private handleSnackBarClose = () => {
        this.setState({ observationToast: false })
    }

    private getImagesFromDB = (images: string) => {
        return localforage.getItem(images)
    }



    private imageClick = (blob: any) => () => {
        const _url = URL.createObjectURL(blob);
        this.props.obsPreviewImage(_url)
    }

    render() {
        return <React.Fragment>
            <Tabs indicatorColor="primary" className="observationCategoryTabs" onChange={this.handleObservationTabChange} scrollable scrollButtons="on" value={this.state.observationCategoryTab}>
                <Tab style={this.getToggleAllObservationStyle(this.props.groupedObservations)} value={0} label="All Observations"></Tab>
                {this.props.categories.map((category, catIndex) => {
                    return <Tab style={this.getToggleSectionStyleObservations(this.props.groupedObservations[catIndex])} key={catIndex} value={catIndex + 1} label={category.name}></Tab>
                })}
            </Tabs>
            <div style={{ position: 'relative' }}>
                <TextField
                    style={{ border: '1px solid grey', width: 'calc(100% - 40px)', background: 'white', boxSizing: 'border-box', margin: '20px' }}
                    InputProps={{ disableUnderline: true, style: { marginLeft: '5px', marginRight: '5px' } }}
                    inputRef={(e) => { this.searchInput = e }}
                    onChange={this.handleSearchInputObservation}
                    placeholder="Search for Observations"
                >
                </TextField>
                {this.searchInput && this.searchInput.value !== "" && <IconButton disableRipple onClick={this.clearSearch} style={{ position: 'absolute', top: 20, right: 20, height: 30, width: 30 }}><CloseIcon></CloseIcon></IconButton>}
            </div>
            <div id="scrollDiv" style={{ maxHeight: 'calc(100vh - 300px)', margin: '0px 20px', WebkitOverflowScrolling: 'touch', overflowY: 'scroll' }}>
                <InfiniteScroll
                    pageStart={1}
                    loadMore={this.loadScroll}
                    hasMore={this.state.hasMoreItems}
                    loader={this.state.observationTotal !== 0 ? <div className="loader" key={0}>Loading ...</div> : <div key={99}></div>}
                    useWindow={false}>
                    {this.state.groupedObservations.length > 0 && this.state.groupedObservations.map((category, catIndex) => {
                        return (this.state.observationCategoryTab === 0 || this.state.observationCategoryTab === this.props.categories.findIndex(cat => category[0] && cat.name === category[0][0].cat) + 1) ?
                            <div key={catIndex} style={{ display: 'block' }}>
                                {category.filter((obs) => {
                                    if (obs[0].text.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].space.value && obs[1].space.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].responsible.value && obs[1].responsible.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].org.value && obs[1].org.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].reqclass.value && obs[1].reqclass.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].action.value && obs[1].action.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].bench.length > 0 && obs[1].bench.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].description.length > 0 && obs[1].description.toLowerCase().includes(this.state.searchValueObs.toLowerCase()) ||
                                        obs[1].risk.value && obs[1].risk.value!.toLowerCase().includes(this.state.searchValueObs.toLowerCase())
                                    ) {
                                        return true;
                                    } else if (obs[1].auditedOrgs.length > 0) {
                                        for (let org of obs[1].auditedOrgs) {
                                            if (org.name && org.name.toLowerCase().includes(this.state.searchValueObs.toLowerCase())) {
                                                return true
                                            }
                                        }
                                    }
                                    return false
                                })
                                    .map((observation, obsIndex, list) => {
                                        return <div style={this.getFlaggedObservationStyle(observation[1])} key={observation[1].id}>
                                            <ObservationInspector
                                                id={this.state.groupedObservations[catIndex].findIndex((obs) => { return observation[1].id === obs[1].id })}
                                                catId={catIndex}
                                                question={observation[0]}
                                                observation={observation[1]}
                                                riskLevels={this.props.riskLevels}
                                                correctionActions={this.props.correctionActions}
                                                responsiblePersons={this.props.responsiblePersons}
                                                labs={this.props.labs}
                                                requestClasses={this.props.requestClasses}
                                                organizations={this.props.organizations}
                                                onDelete={this.handleObservationDelete}
                                                onChange={this.handleObservationChange}
                                                onCopy={this.handleObservationCopy}
                                                auditedDepartments={observation[1].auditedOrgs}
                                                handleFlaggedChange={this.handleFlaggedChange}
                                                handleOrganizationChange={this.handleOrganizationChange}
                                                handleLabChange={this.handleLabChange}
                                                handleRiskChange={this.handleRiskChange}
                                                handleCorrectiveActionChange={this.handleCorrectiveActionChange}
                                                handleResponsiblePersonChange={this.handleResponsiblePersonChange}
                                                handleRequestClassChange={this.handleRequestClassChange}
                                                handleBenchChange={this.handleBenchChange}
                                                handleDescriptionSelect={this.handleDescriptionSelect}
                                                handleDescriptionChange={this.handleDescriptionChange}
                                                handleAuditedDepartmentsChange={this.handleAuditedDepartmentsChange}
                                                urls={this.props.urls}
                                                getImages={this.getImagesFromDB}
                                                handleImagesChange={this.handleImagesChange}
                                                handleImageDelete={this.handleImageDelete}
                                                handleObservationContinue={this.handleObservationContinue}
                                                imageClick={this.imageClick}
                                                setDescriptionRows={this.setDescriptionRows}
                                            />
                                        </div>
                                    }).slice(0, this.state.scrollPage * 5)}

                            </div> : null
                    })}
                </InfiniteScroll>
            </div>
            <SnackBar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                onClose={this.handleSnackBarClose}
                autoHideDuration={2000}
                onClick={this.handleSnackBarClose}
                open={this.state.observationToast}
                message={<span id="message-id">{this.state.messageContent}</span>}
            />
            <Dialog open={this.state.openDialog} onClose={this.handleAlertDialogClose}>
                <DialogTitle>
                    Please complete all fields before unflagging observation.
                </DialogTitle>
                <DialogActions>
                    <ButtonMaterial variant="raised" onClick={this.handleAlertDialogClose} disableRipple>Ok</ButtonMaterial>
                </DialogActions>
            </Dialog>

        </React.Fragment>;

    }
}
export default ObservationsTab;