import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import postcss from 'rollup-plugin-postcss';
import url from "rollup-plugin-url";

const dev = 'development';
const prod = 'production';
const env = (process.env.NODE_ENV === prod || process.env.NODE_ENV === dev) ?
  process.env.NODE_ENV : dev;

const plugins = [
  replace({
    'process.env.NODE_ENV': JSON.stringify(env)
  }),
  resolve(),
  url({
    limit: 10 * 1024, // inline files < 10k, copy files > 10k
   // defaults to .svg, .png, .jpg and .gif files
    emitFiles: true // defaults to true
  }),
  postcss({
    plugins: []
  }),
  commonjs({
    include: ['node_modules/**'],
    namedExports: {
      'node_modules/react-dom/index.js': [
        'render',
        'findDOMNode'
      ],
      'node_modules/react/index.js': [
        'Component',
        'PureComponent',
        'PropTypes',
        'createElement',
        'Fragment',
        'cloneElement',
        'Children'
      ],
      'node_modules/localforage/dist/localforage.js': [
        'INDEXEDDB', 'setDriver', 'setItem', 'supports', 'config'
      ],
      'node_modules/lodash/lodash.js': [
        'uniqBy','groupBy','unionBy','isEqual','cloneDeep','debounce'
      ],
      'node_modules/bluebird/js/release/bluebird.js':[
        'each','then'
      ]
    },
  }),
];

export default {
  plugins,
  input: './build/index.js',
  output: {
    sourcemap: false,
    file: './lab-audit-dev/index.js',
    format: 'iife'
  },
  onwarn: function (warning) {
    if (warning.code === 'THIS_IS_UNDEFINED')
      return;

    console.warn(warning.message);
  }
};